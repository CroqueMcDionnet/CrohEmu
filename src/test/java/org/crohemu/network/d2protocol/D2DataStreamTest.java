package org.crohemu.network.d2protocol;

import org.junit.Assert;
import org.junit.Test;

public class D2DataStreamTest {
    @Test
    public void writeReadVariableLengthInt() throws Exception {
        D2DataOutputStream out = new D2DataOutputStream();
        out.writeVariableLengthInt(Integer.MAX_VALUE);
        out.writeVariableLengthInt(3000);

        D2DataInputStream in = new D2DataInputStream(out.getUnderlyingStream().toByteArray());
        Assert.assertEquals(Integer.MAX_VALUE, in.readVariableLengthInt());
        Assert.assertEquals(3000, in.readVariableLengthInt());
    }

    @Test
    public void writeReadVariableLengthShort() throws Exception {
        D2DataOutputStream out = new D2DataOutputStream();
        out.writeVariableLengthShort(Short.MAX_VALUE);
        out.writeVariableLengthShort(450);
        out.writeVariableLengthShort(0);

        D2DataInputStream in = new D2DataInputStream(out.getUnderlyingStream().toByteArray());
        Assert.assertEquals(Short.MAX_VALUE, in.readVariableLengthShort());
        Assert.assertEquals(450, in.readVariableLengthShort());
        Assert.assertEquals(0, in.readVariableLengthShort());
    }

    @Test
    public void writeReadVariableLengthLong() throws Exception {
        D2DataOutputStream out = new D2DataOutputStream();
        out.writeVariableLengthLong(92233720368547758L);
        out.writeVariableLengthLong(126);
        out.writeVariableLengthLong(1260000);
        out.writeVariableLengthLong(-1260000);
        out.writeVariableLengthLong(8379895309L);
        out.writeVariableLengthLong(0b00000001_00000000_00000000_00000000_00000000L);
        out.writeVariableLengthLong(262295608698276663L);
        out.writeVariableLengthLong(90071992547990L);

        D2DataInputStream in = new D2DataInputStream(out.getUnderlyingStream().toByteArray());
        Assert.assertEquals(92233720368547758L, in.readVariableLengthLong());
        Assert.assertEquals(126, in.readVariableLengthLong());
        Assert.assertEquals(1260000, in.readVariableLengthLong());
        Assert.assertEquals(-1260000, in.readVariableLengthLong());
        Assert.assertEquals(8379895309L, in.readVariableLengthLong());
        Assert.assertEquals(0b00000001_00000000_00000000_00000000_00000000L, in.readVariableLengthLong());
        Assert.assertEquals(262295608698276663L, in.readVariableLengthLong());
        Assert.assertEquals(90071992547990L, in.readVariableLengthLong());
    }
}