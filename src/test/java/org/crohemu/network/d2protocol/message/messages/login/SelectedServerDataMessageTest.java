package org.crohemu.network.d2protocol.message.messages.login;

import org.crohemu.network.tcpwrapper.TcpClient;
import org.junit.Assert;
import org.junit.Test;

public class SelectedServerDataMessageTest {

    @Test
    public void serializeDeserializeTest() throws Exception {
        final int[] TICKET = {1, 2, 3, 4};
        final int SERVER_ID = 1;
        final String ADDRESS = "127.0.0.1";
        final int PORT = 11235;
        final boolean CAN_CREATE_NEW_CHARACTER = true;


        SelectedServerDataMessage messageOut = new SelectedServerDataMessage();
        messageOut.setTicket(TICKET);
        messageOut.setServerId(SERVER_ID);
        messageOut.setAddress(ADDRESS);
        messageOut.setPort(PORT);
        messageOut.setCanCreateNewCharacter(CAN_CREATE_NEW_CHARACTER);
        messageOut.pack();

        SelectedServerDataMessage messageIn = new SelectedServerDataMessage(messageOut.getRawContent(), new TcpClient(null));
        Assert.assertArrayEquals(TICKET, messageIn.getTicket());
        Assert.assertEquals(SERVER_ID, messageIn.getServerId());
        Assert.assertEquals(ADDRESS, messageIn.getAddress());
        Assert.assertEquals(PORT, messageIn.getPort());
        Assert.assertEquals(CAN_CREATE_NEW_CHARACTER, messageIn.canCreateNewCharacter());
    }

}