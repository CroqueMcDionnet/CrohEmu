package org.crohemu.network.tcpwrapper.impl;

import org.crohemu.network.tcpwrapper.TcpClient;
import org.crohemu.network.tcpwrapper.TcpServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashSet;
import java.util.Set;

@Component
public class TcpServerImpl implements TcpServer {
    final private Logger logger = LoggerFactory.getLogger(TcpServerImpl.class);

    private ServerSocket serverSocket;

    private Set<TcpClient> clients = new HashSet<>();

    protected void onClientConnected(TcpClient client) {}

    protected void onClientClosedConnection(TcpClient client) {}

    protected byte[] readMessage(InputStream socketStream) throws IOException { return new byte[0]; }

    protected void handleMessage(byte[] messageData, TcpClient source) {}

    @Override
    public void start(InetAddress ipAddress, int portNumber) throws IOException {
        serverSocket = new ServerSocket(portNumber, Integer.MAX_VALUE, ipAddress);

            new Thread(() -> {
                while (true) {
                    Socket clientSocket = null;
                    try {
                        clientSocket = serverSocket.accept();
                    } catch (IOException e) {
                        logger.error("Error while listening for connections on {}:{}", ipAddress, portNumber);
                        continue;
                    }

                    TcpClient client = new TcpClient(clientSocket);
                    clients.add(client);
                    this.onClientConnected(client);
                    new Thread(() -> {
                        byte[] messageData = new byte[0];
                            while (clients.contains(client)) {
                                try {
                                    messageData = this.readMessage(client.getSocket().getInputStream());
                                } catch (IOException e) {
                                    logger.error("Exception while reading tcp data from {}: {}", client, e.getMessage());
                                    disconnectClient(client);
                                }

                                // connection closed
                                if (messageData.length == 0) {
                                    this.onClientClosedConnection(client);
                                    disconnectClient(client);
                                } else {
                                    this.handleMessage(messageData, client);
                                }
                            }
                    }).start();
                }
            }).start();
    }

    @Override
    public void disconnectClient(TcpClient client) {
        clients.remove(client);
        try {
            client.disconnect();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Set<TcpClient> getClients() {
        return this.clients;
    }
}
