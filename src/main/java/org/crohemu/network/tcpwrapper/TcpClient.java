package org.crohemu.network.tcpwrapper;

import com.sun.org.apache.xerces.internal.impl.dv.util.HexBin;
import org.crohemu.server.login.config.LoginServerConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.Socket;

public class TcpClient {
    Logger logger = LoggerFactory.getLogger(TcpClient.class);

    Socket socket;

    private int localSequenceNumber = 0;
    private int remoteSequenceNumber = 0;

    public TcpClient(Socket socket) {
        this.socket = socket;
    }

    public void sendMessage(TcpMessage message) throws IOException {
        message.pack();
        byte[] messageContent = message.getRawContent();

        if (LoginServerConfig.execModeLessThan(LoginServerConfig.ExecMode.DEBUG0)) {
            logger.debug("[{}} ---> {} ({})", socket.getInetAddress(), message.getClass().getSimpleName(), HexBin.encode(messageContent));
        }
        socket.getOutputStream().write(messageContent, 0, messageContent.length);
        localSequenceNumber++;
    }

    public void disconnect() throws IOException {
        socket.close();
    }

    public Socket getSocket() {
        return socket;
    }

    @Override
    public String toString() {
        return getIpAddress();
    }

    private String getIpAddress() {
        return socket.getInetAddress().toString();
    }


    public int getLocalSequenceNumber() {
        return localSequenceNumber;
    }

    public int getRemoteSequenceNumber() {
        return remoteSequenceNumber;
    }

    public void incrementRemoteSequenceNumber() {
        remoteSequenceNumber++;
    }
}
