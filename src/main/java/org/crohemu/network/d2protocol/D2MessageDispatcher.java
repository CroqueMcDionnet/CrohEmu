package org.crohemu.network.d2protocol;

import org.crohemu.network.d2protocol.message.D2Message;

@FunctionalInterface
public interface D2MessageDispatcher {
    void dispatchD2Message(D2Message message, D2Client client);
}
