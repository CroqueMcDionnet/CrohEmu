package org.crohemu.network.d2protocol;

import org.crohemu.network.tcpwrapper.handler.TcpMessageReader;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;

@Component
public class D2MessageReader implements TcpMessageReader {
    private static final int HIGH_HEADER_LENGTH = 2;

    @Override
    public byte[] readTcpMessage(InputStream dataStream) throws IOException {
        byte[] fullMessageBuffer = new byte[0];
        int totalBytesRead = 0;
        boolean fullMessageHasBeenRead = false;
        int messageTypeLen = 0;
        int messageLength = -1;

        // what we read in an iteration. Its size fluctuates.
        byte[] messagePartBuffer = new byte[HIGH_HEADER_LENGTH];

        // next number of bytes to read
        int nextChunkSize = 0;

        while (!fullMessageHasBeenRead) {
            int bytesRead = dataStream.read(messagePartBuffer);
            if (bytesRead == -1) {
                break;
            }

            // data is D2 high header
            if (totalBytesRead == 0) {
                messageTypeLen = messagePartBuffer[1] & 0b00000011;
                nextChunkSize = messageTypeLen;
            }
            // data is D2 messageLength
            else if (totalBytesRead == HIGH_HEADER_LENGTH) {
                switch (messageTypeLen) {
                    case 0:
                        messageLength = 0;
                        break;
                    case 1:
                        messageLength = messagePartBuffer[0];
                        break;
                    case 2:
                        messageLength = messagePartBuffer[0] << 8 | messagePartBuffer[1];
                        break;
                    case 3:
                        messageLength = messagePartBuffer[0] << 16 | messagePartBuffer[1] << 8 | messagePartBuffer[0];
                }

                nextChunkSize = messageLength;
            }
            // else data is message content. We just add it to the result buffer.

            totalBytesRead += bytesRead;
            if (messageLength != -1 && totalBytesRead == messageLength + messageTypeLen + HIGH_HEADER_LENGTH) {
                fullMessageHasBeenRead = true;
            }

            // add bytes read to full message
            byte[] newFullMessageBuffer = new byte[fullMessageBuffer.length + bytesRead];
            System.arraycopy(fullMessageBuffer, 0, newFullMessageBuffer, 0, fullMessageBuffer.length);
            System.arraycopy(messagePartBuffer, 0, newFullMessageBuffer, fullMessageBuffer.length, bytesRead);
            fullMessageBuffer = newFullMessageBuffer;

            messagePartBuffer = new byte[nextChunkSize];
        }

        return fullMessageBuffer;
    }
}
