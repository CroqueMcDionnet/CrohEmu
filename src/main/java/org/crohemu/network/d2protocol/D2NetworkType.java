package org.crohemu.network.d2protocol;

public interface D2NetworkType {
    void serialize(D2DataOutputStream outputStream);
    void deserialize(D2DataInputStream inputStream);
    boolean isValid();
}
