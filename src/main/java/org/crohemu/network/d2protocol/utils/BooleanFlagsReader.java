package org.crohemu.network.d2protocol.utils;

/**
 * Reads bits from a variable as flags.
 */
public class BooleanFlagsReader {

    /**
     * Reads flags from a byte.
     * @param flags The byte that contains the flags values.
     * @param position The position at which to read the flag. 0 is the LSB. 7 is the MSB.
     * @return true if bit <i>position</i> is set in <i>flags</i>, otherwise false.
     */
    public static boolean readFlag(byte flags, int position) {
        return ((flags >> position) & 1) == 1;
    }
}
