package org.crohemu.network.d2protocol.message.messages.types;

import org.crohemu.network.d2protocol.D2DataInputStream;
import org.crohemu.network.d2protocol.D2DataOutputStream;
import org.crohemu.network.d2protocol.D2NetworkType;

import java.io.IOException;

/**
 * Information on a game server for a specific Account.
 */
public class GameServerInformation implements D2NetworkType {

    /**
     * The server's functional Id.
     */
    private int id;

    /**
     * The server's type : Normal, Epic, Heroic...
     * TODO create an enum for this
     */
    private int type = -1;

    /**
     * The server's status.
     */
    private int status = 1;

    /**
     * An indicator of the amount of created characters on the server.
     * TODO create an enum for this
     */
    private int completion = 0;

    /**
     * Is the account allowed to select the server.
     * TODO use it correctly
     */
    private boolean isSelectable = false;

    /**
     * The number of characters the account has created on the server.
     */
    private int charactersCount = 0;

    /**
     * The total number of characters the account is allowed to create on the server.
     */
    private int charactersSlots = 0;

    /**
     * The server's opening date.
     */
    private long date = 0;

    public GameServerInformation() {
    }

    public void serialize(D2DataOutputStream out) {
        try {
            out.writeVariableLengthShort(id);
            out.writeByte(type);
            out.writeByte(status);
            out.writeByte(completion);
            out.writeBoolean(isSelectable);
            out.writeByte(charactersCount);
            out.writeByte(charactersSlots);
            out.writeDouble(date);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deserialize(D2DataInputStream inputStream) {
        // never received
    }

    @Override
    public boolean isValid() {
        return id > 0 &&
                type > 0 &&
                status >= 0 && status <= 7 &&
                completion >= 0 && completion <= 4 &&
                charactersCount >= 0 &&
                charactersSlots >= 0 &&
                date >= 0;
    }

    public enum GameServerStatus {
        UNKNOWN(0),
        OFFLINE(1),
        STARTING(2),
        ONLINE(3),
        NOJOIN(4),
        SAVING(5),
        STOPPING(6),
        FULL(7);

        public int getCode() {
            return code;
        }

        int code;
        GameServerStatus(int code) {
            this.code = code;
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getCompletion() {
        return completion;
    }

    public void setCompletion(int completion) {
        this.completion = completion;
    }

    public boolean isSelectable() {
        return isSelectable;
    }

    public void setSelectable(boolean selectable) {
        isSelectable = selectable;
    }

    public int getCharactersCount() {
        return charactersCount;
    }

    public void setCharactersCount(int charactersCount) {
        this.charactersCount = charactersCount;
    }

    public int getCharactersSlots() {
        return charactersSlots;
    }

    public void setCharactersSlots(int charactersSlots) {
        this.charactersSlots = charactersSlots;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }
}
