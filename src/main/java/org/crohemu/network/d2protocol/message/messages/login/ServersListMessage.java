package org.crohemu.network.d2protocol.message.messages.login;

import org.crohemu.network.d2protocol.D2DataOutputStream;
import org.crohemu.network.d2protocol.message.D2Message;
import org.crohemu.network.d2protocol.message.messages.D2MessageType;
import org.crohemu.network.d2protocol.message.messages.types.GameServerInformation;

import java.io.IOException;
import java.util.List;

/**
 * Sent to the client. Contains information about the game servers.
 */
public class ServersListMessage extends D2Message{

    /**
     * The game servers' information.
     */
    private List<GameServerInformation> servers;

    /**
     * The server id to which the client is already connected.
     * TODO use it
     */
    private int alreadyConnectedToServerId;

    /**
     * Is the client allowed to create a character ?
     * TODO use it, for example cap an account's max characters as in the official game
     */
    private boolean canCreateNewCharacter;

    public ServersListMessage() {

    }

    @Override
    public void serialize(D2DataOutputStream outputStream) {
        try {
            outputStream.writeShort(servers.size());
            servers.forEach(gameServerInformation -> gameServerInformation.serialize(outputStream));
            outputStream.writeVariableLengthShort(alreadyConnectedToServerId);
            outputStream.writeBoolean(canCreateNewCharacter);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<GameServerInformation> getServers() {
        return servers;
    }

    public void setServers(List<GameServerInformation> servers) {
        this.servers = servers;
    }

    public int getAlreadyConnectedToServerId() {
        return alreadyConnectedToServerId;
    }

    public void setAlreadyConnectedToServerId(int alreadyConnectedToServerId) {
        this.alreadyConnectedToServerId = alreadyConnectedToServerId;
    }

    public boolean isCanCreateNewCharacter() {
        return canCreateNewCharacter;
    }

    public void setCanCreateNewCharacter(boolean canCreateNewCharacter) {
        this.canCreateNewCharacter = canCreateNewCharacter;
    }
}
