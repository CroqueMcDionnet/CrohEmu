package org.crohemu.network.d2protocol.message.messages.types.fight;

import org.crohemu.network.d2protocol.D2DataInputStream;
import org.crohemu.network.d2protocol.D2DataOutputStream;
import org.crohemu.network.d2protocol.D2NetworkType;

import java.io.IOException;
import java.util.List;

public class FightCommonInformation implements D2NetworkType {
    private int fightId;
    private byte fightType;
    private List<FightTeamInformation> fightTeams;
    private List<Short> fightTeamPositions;
    private List<FightOptionsInformation> fightTeamOptions;

    @Override
    public void serialize(D2DataOutputStream outputStream) {
        try {
            outputStream.writeInt(fightId);
            outputStream.writeByte(fightType);
            outputStream.writeShort(fightTeams.size());
            for (FightTeamInformation info : fightTeams) {
                outputStream.writeShort(info.getTypeId());
                info.serialize(outputStream);
            }
            outputStream.writeShort(fightTeamPositions.size());
            for (Short position :  fightTeamPositions) {
                outputStream.writeVariableLengthShort(position);
            }
            outputStream.writeShort(fightTeamOptions.size());
            for (FightOptionsInformation info : fightTeamOptions) {
                info.serialize(outputStream);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deserialize(D2DataInputStream inputStream) {
        // unused
    }

    @Override
    public boolean isValid() {
        return true;
    }

    public int getFightId() {
        return fightId;
    }

    public void setFightId(int fightId) {
        this.fightId = fightId;
    }

    public byte getFightType() {
        return fightType;
    }

    public void setFightType(byte fightType) {
        this.fightType = fightType;
    }

    public List<FightTeamInformation> getFightTeams() {
        return fightTeams;
    }

    public void setFightTeams(List<FightTeamInformation> fightTeams) {
        this.fightTeams = fightTeams;
    }

    public List<Short> getFightTeamPositions() {
        return fightTeamPositions;
    }

    public void setFightTeamPositions(List<Short> fightTeamPositions) {
        this.fightTeamPositions = fightTeamPositions;
    }

    public List<FightOptionsInformation> getFightTeamOptions() {
        return fightTeamOptions;
    }

    public void setFightTeamOptions(List<FightOptionsInformation> fightTeamOptions) {
        this.fightTeamOptions = fightTeamOptions;
    }
}
