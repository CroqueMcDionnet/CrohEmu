package org.crohemu.network.d2protocol.message.messages.login;

import org.crohemu.network.d2protocol.D2DataInputStream;
import org.crohemu.network.d2protocol.message.D2Message;
import org.crohemu.network.d2protocol.message.messages.D2MessageType;
import org.crohemu.network.tcpwrapper.TcpClient;

import java.io.IOException;

/**
 * Sent by the client when selecting a game server.
 */
public class ServerSelectionMessage extends D2Message {

    private int serverId;

    public ServerSelectionMessage(byte[] messageData) {
        super(messageData);

    }

    @Override
    protected void deserialize(D2DataInputStream inputStream) {
        try {
            serverId = inputStream.readVariableLengthShort();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean isValid() {
        return serverId >= 0;
    }

    public int getServerId() {
        return serverId;
    }
}
