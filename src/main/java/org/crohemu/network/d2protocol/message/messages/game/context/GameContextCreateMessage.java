package org.crohemu.network.d2protocol.message.messages.game.context;

import org.crohemu.network.d2protocol.D2DataOutputStream;
import org.crohemu.network.d2protocol.message.D2Message;

import java.io.IOException;

public class GameContextCreateMessage extends D2Message {

    public static final byte TYPE_ROLE_PLAY = 1;
    public static final byte TYPE_FIGHT = 2;

    private byte type;

    public GameContextCreateMessage(byte type) {
        super();
        this.type = type;
    }

    @Override
    protected void serialize(D2DataOutputStream outputStream) {
        try {
            outputStream.writeByte(type);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public byte getType() {
        return type;
    }

    public void setType(byte type) {
        this.type = type;
    }
}
