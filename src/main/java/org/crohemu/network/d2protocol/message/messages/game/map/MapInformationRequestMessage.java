package org.crohemu.network.d2protocol.message.messages.game.map;

import org.crohemu.network.d2protocol.D2DataInputStream;
import org.crohemu.network.d2protocol.message.D2Message;

import java.io.IOException;

public class MapInformationRequestMessage extends D2Message {

    private int mapId;

    public MapInformationRequestMessage(byte[] messageData) {
        super(messageData);
    }

    @Override
    protected void deserialize(D2DataInputStream inputStream) {
        try {
            mapId = inputStream.readInt();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean isValid() {
        return mapId >= 0;
    }

    public int getMapId() {
        return mapId;
    }

    public void setMapId(int mapId) {
        this.mapId = mapId;
    }
}
