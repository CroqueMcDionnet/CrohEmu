package org.crohemu.network.d2protocol.message.messages.login;

import org.crohemu.network.d2protocol.D2DataOutputStream;
import org.crohemu.network.d2protocol.message.D2Message;
import org.crohemu.network.d2protocol.message.messages.D2MessageType;
import org.crohemu.network.d2protocol.message.messages.types.IdentificationFailedReason;

import java.io.IOException;

/**
 * Sent when a client tried to connect but it failed.
 */
public class IdentificationFailedMessage extends D2Message {

    /**
     * TODO create enum for reason and use it
     */
    private int reason;

    public IdentificationFailedMessage(IdentificationFailedReason reason) {

        this.reason = reason.getCode();
    }

    @Override
    protected void serialize(D2DataOutputStream outputStream) {
        try {
            outputStream.writeByte(reason);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public int getReason() {
        return reason;
    }

    public void setReason(int reason) {
        this.reason = reason;
    }
}
