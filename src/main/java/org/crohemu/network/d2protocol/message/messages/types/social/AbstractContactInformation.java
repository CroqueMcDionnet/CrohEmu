package org.crohemu.network.d2protocol.message.messages.types.social;

import org.apache.commons.lang3.StringUtils;
import org.crohemu.network.d2protocol.D2DataInputStream;
import org.crohemu.network.d2protocol.D2DataOutputStream;
import org.crohemu.network.d2protocol.D2NetworkType;

import java.io.IOException;

public class AbstractContactInformation implements D2NetworkType {

    public int accountId = 0;
    public String accountName = "";


    @Override
    public void serialize(D2DataOutputStream outputStream) {
        try {
            outputStream.writeInt(accountId);
            outputStream.writeUTF(accountName);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deserialize(D2DataInputStream inputStream) {
        try {
            accountId = inputStream.readInt();
            accountName = inputStream.readUTF();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean isValid() {
        return accountId >= 0 && StringUtils.isNotBlank(accountName);
    }
}
