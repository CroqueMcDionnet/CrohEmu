package org.crohemu.network.d2protocol.message.messages.types.actor;

import org.crohemu.network.d2protocol.D2DataOutputStream;

import java.io.IOException;

public class GameRolePlayNamedActorInformation extends GameRolePlayActorInformation {

    private String name;

    @Override
    public int getTypeId() {
        return 154;
    }

    @Override
    public void serialize(D2DataOutputStream outputStream) {
        super.serialize(outputStream);
        try {
            outputStream.writeUTF(name);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean isValid() {
        return super.isValid();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
