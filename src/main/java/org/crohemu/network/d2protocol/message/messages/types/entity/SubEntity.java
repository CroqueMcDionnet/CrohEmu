package org.crohemu.network.d2protocol.message.messages.types.entity;

import org.crohemu.network.d2protocol.D2DataInputStream;
import org.crohemu.network.d2protocol.D2DataOutputStream;
import org.crohemu.network.d2protocol.D2NetworkType;

import java.io.IOException;

public class SubEntity implements D2NetworkType{
    private int bindingPointCategory = 0;
    private int bindingPointIndex = 0;
    private EntityLook subEntityLook = new EntityLook();

    public int getBindingPointCategory() {
        return bindingPointCategory;
    }

    public void setBindingPointCategory(int bindingPointCategory) {
        this.bindingPointCategory = bindingPointCategory;
    }

    public int getBindingPointIndex() {
        return bindingPointIndex;
    }

    public void setBindingPointIndex(int bindingPointIndex) {
        this.bindingPointIndex = bindingPointIndex;
    }

    public EntityLook getSubEntityLook() {
        return subEntityLook;
    }

    public void setSubEntityLook(EntityLook subEntityLook) {
        this.subEntityLook = subEntityLook;
    }

    @Override
    public void serialize(D2DataOutputStream outputStream) {
        try {
            outputStream.writeByte(bindingPointCategory);
            outputStream.writeByte(bindingPointIndex);
            subEntityLook.serialize(outputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deserialize(D2DataInputStream inputStream) {
        // only for testing purposes

        try {
            this.bindingPointCategory = inputStream.readByte();
            this.bindingPointIndex = inputStream.readByte();

            this.subEntityLook = new EntityLook();
            this.subEntityLook.deserialize(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean isValid() {
        return bindingPointCategory >= 0
                && bindingPointIndex >= 0
                && subEntityLook.isValid();
    }
}
