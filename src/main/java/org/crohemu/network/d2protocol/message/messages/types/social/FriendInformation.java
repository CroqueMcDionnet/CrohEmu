package org.crohemu.network.d2protocol.message.messages.types.social;

import org.crohemu.network.d2protocol.D2DataInputStream;
import org.crohemu.network.d2protocol.D2DataOutputStream;
import org.crohemu.network.d2protocol.D2NetworkType;

import java.io.IOException;

public class FriendInformation extends AbstractContactInformation implements D2NetworkType {

    public final static int STATE_NOT_CONNECTED = 0;
    public final static int STATE_IN_ROLEPLAY = 1;
    public final static int STATE_IN_FIGHT = 2;
    public final static int STATE_UNKNOWN = 99;

    public int playerState = 99;
    public int lastConnection = 0;
    public int achievementPoints = 0;

    @Override
    public void serialize(D2DataOutputStream outputStream) {
        super.serialize(outputStream);
        try {
            outputStream.writeByte(playerState);
            outputStream.writeVariableLengthShort(lastConnection);
            outputStream.writeInt(achievementPoints);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deserialize(D2DataInputStream inputStream) {
        super.deserialize(inputStream);
        try {
            playerState = inputStream.readByte();
            lastConnection = inputStream.readVariableLengthShort();
            achievementPoints = inputStream.readInt();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean isValid() {
        return super.isValid() && playerState >= 0 && playerState < 100 && lastConnection >= 0 && achievementPoints >= 0;
    }
}
