package org.crohemu.network.d2protocol.message.messages.game.social;

import org.crohemu.network.d2protocol.D2DataOutputStream;
import org.crohemu.network.d2protocol.message.D2Message;
import org.crohemu.network.d2protocol.message.messages.D2MessageType;
import org.crohemu.network.d2protocol.message.messages.types.social.FriendInformation;

import java.io.IOException;
import java.util.List;

public class FriendsListMessage extends D2Message {

    List<FriendInformation> friendInformationList;

    public FriendsListMessage() {

    }

    public FriendsListMessage(List<FriendInformation> friendInformationList) {
        this();
        this.friendInformationList = friendInformationList;
    }

    @Override
    protected void serialize(D2DataOutputStream outputStream) {
        try {
            outputStream.writeShort(friendInformationList.size());
            for (FriendInformation info : friendInformationList) {
                info.serialize(outputStream);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<FriendInformation> getFriendInformationList() {
        return friendInformationList;
    }

    public void setFriendInformationList(List<FriendInformation> friendInformationList) {
        this.friendInformationList = friendInformationList;
    }
}
