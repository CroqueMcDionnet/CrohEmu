package org.crohemu.network.d2protocol.message.messages.types.map;

import org.crohemu.network.d2protocol.D2DataInputStream;
import org.crohemu.network.d2protocol.D2DataOutputStream;
import org.crohemu.network.d2protocol.D2NetworkType;
import org.crohemu.network.d2protocol.utils.BooleanFlagsWriter;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

public class HouseInformation implements D2NetworkType {

    private boolean isOnSale = false;
    private boolean isSaleLocked = false;
    private int houseId = 0;
    private List<Integer> doorsOnMap = Collections.emptyList();
    private String ownerName = "Owner unkwnown";
    private short modelId = 0;

    @Override
    public void serialize(D2DataOutputStream outputStream) {
        try {
            outputStream.writeByte(BooleanFlagsWriter.flagsToByte(isOnSale, isSaleLocked));
            outputStream.writeVariableLengthInt(houseId);
            outputStream.writeShort(doorsOnMap.size());
            for (Integer door : doorsOnMap) {
                outputStream.writeInt(door);
            }
            outputStream.writeUTF(ownerName);
            outputStream.writeVariableLengthShort(modelId);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deserialize(D2DataInputStream inputStream) {
        // unused
    }

    @Override
    public boolean isValid() {
        return true;
    }

    public boolean isOnSale() {
        return isOnSale;
    }

    public void setOnSale(boolean onSale) {
        isOnSale = onSale;
    }

    public boolean isSaleLocked() {
        return isSaleLocked;
    }

    public void setSaleLocked(boolean saleLocked) {
        isSaleLocked = saleLocked;
    }

    public int getHouseId() {
        return houseId;
    }

    public void setHouseId(int houseId) {
        this.houseId = houseId;
    }

    public List<Integer> getDoorsOnMap() {
        return doorsOnMap;
    }

    public void setDoorsOnMap(List<Integer> doorsOnMap) {
        this.doorsOnMap = doorsOnMap;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public short getModelId() {
        return modelId;
    }

    public void setModelId(short modelId) {
        this.modelId = modelId;
    }

    public int getTypeId() {
        return 111;
    }
}
