package org.crohemu.network.d2protocol.message.messages.login.character;

import org.crohemu.network.d2protocol.D2DataOutputStream;
import org.crohemu.network.d2protocol.message.D2Message;
import org.crohemu.network.d2protocol.message.messages.D2MessageType;

import java.io.IOException;

public class CharacterDeletionErrorMessage extends D2Message {

    public static final int REASON_NO_REASON = 1;
    public static final int REASON_TOO_MANY_CHARACTER_DELETION = 2;
    public static final int REASON_WRONG_SECRET_ANSWER = 3;
    public static final int REASON_RESTRICTED_ZONE = 4;

    int reason;

    public CharacterDeletionErrorMessage(int reason) {
        super();
        this.reason = reason;

    }

    @Override
    protected void serialize(D2DataOutputStream outputStream) {
        try {
            outputStream.writeByte(reason);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
