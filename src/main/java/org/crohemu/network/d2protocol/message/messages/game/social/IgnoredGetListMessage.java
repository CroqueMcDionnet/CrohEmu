package org.crohemu.network.d2protocol.message.messages.game.social;

import org.crohemu.network.d2protocol.message.D2Message;
import org.crohemu.network.d2protocol.message.messages.D2MessageType;

public class IgnoredGetListMessage extends D2Message {

    public IgnoredGetListMessage(byte[] messageData) {
        super(messageData);

    }
}
