package org.crohemu.network.d2protocol.message.messages.game.auth;

import org.crohemu.network.d2protocol.D2DataOutputStream;
import org.crohemu.network.d2protocol.message.D2Message;
import org.crohemu.network.d2protocol.message.messages.D2MessageType;

import java.io.IOException;

public class RawDataMessage extends D2Message {

    private byte[] data;

    public RawDataMessage(byte[] data) {
        this.data = data;

    }

    @Override
    protected void serialize(D2DataOutputStream outputStream) {
        try {
            outputStream.writeVariableLengthInt(data.length);
            for (byte dataByte : data) {
                outputStream.writeByte(dataByte);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
