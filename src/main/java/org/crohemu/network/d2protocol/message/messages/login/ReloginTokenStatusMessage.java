package org.crohemu.network.d2protocol.message.messages.login;

import org.crohemu.network.d2protocol.D2DataOutputStream;
import org.crohemu.network.d2protocol.message.D2Message;
import org.crohemu.network.d2protocol.message.messages.D2MessageType;

import java.io.IOException;

public class ReloginTokenStatusMessage extends D2Message {

    private boolean validToken;
    private int[] ticket;

    public ReloginTokenStatusMessage() {
        super();

    }

    @Override
    protected void serialize(D2DataOutputStream outputStream) {
        try {
            outputStream.writeBoolean(validToken);
            outputStream.writeVariableLengthInt(ticket.length);
            for (Integer i : ticket) {
                outputStream.writeByte(i);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean isValidToken() {
        return validToken;
    }

    public void setValidToken(boolean validToken) {
        this.validToken = validToken;
    }

    public int[] getTicket() {
        return ticket;
    }

    public void setTicket(int[] ticket) {
        this.ticket = ticket;
    }
}
