package org.crohemu.network.d2protocol.message.messages.types.map;

import org.crohemu.network.d2protocol.D2DataInputStream;
import org.crohemu.network.d2protocol.D2DataOutputStream;
import org.crohemu.network.d2protocol.D2NetworkType;

import java.io.IOException;

public class StatedElement implements D2NetworkType{

    private int elementId;
    private short elementCellId;
    private int elementState;

    @Override
    public void serialize(D2DataOutputStream outputStream) {
        try {
            outputStream.writeInt(elementId);
            outputStream.writeVariableLengthShort(elementCellId);
            outputStream.writeVariableLengthInt(elementState);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deserialize(D2DataInputStream inputStream) {

    }

    @Override
    public boolean isValid() {
        return elementId >= 0 && elementCellId >= 0 && elementCellId <= 559 && elementCellId >= 0;
    }

    public int getElementId() {
        return elementId;
    }

    public void setElementId(int elementId) {
        this.elementId = elementId;
    }

    public short getElementCellId() {
        return elementCellId;
    }

    public void setElementCellId(short elementCellId) {
        this.elementCellId = elementCellId;
    }

    public int getElementState() {
        return elementState;
    }

    public void setElementState(int elementState) {
        this.elementState = elementState;
    }
}
