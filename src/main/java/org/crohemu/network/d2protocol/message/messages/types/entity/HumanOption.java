package org.crohemu.network.d2protocol.message.messages.types.entity;

import org.crohemu.network.d2protocol.D2DataInputStream;
import org.crohemu.network.d2protocol.D2DataOutputStream;
import org.crohemu.network.d2protocol.D2NetworkType;

public class HumanOption implements D2NetworkType {

    public int getTypeId() { return 406; }

    @Override
    public void serialize(D2DataOutputStream outputStream) {

    }

    @Override
    public void deserialize(D2DataInputStream inputStream) {

    }

    @Override
    public boolean isValid() {
        return true;
    }
}
