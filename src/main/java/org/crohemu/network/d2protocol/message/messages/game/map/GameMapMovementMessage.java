package org.crohemu.network.d2protocol.message.messages.game.map;

import org.crohemu.network.d2protocol.D2DataOutputStream;
import org.crohemu.network.d2protocol.message.D2Message;

import java.io.IOException;
import java.util.List;

public class GameMapMovementMessage extends D2Message {
    private List<Short> keyMovements;
    private double actorId;

    public GameMapMovementMessage(List<Short> keyMovements, double actorId) {
        this.keyMovements = keyMovements;
        this.actorId = actorId;
    }

    @Override
    protected void serialize(D2DataOutputStream outputStream) {
        try {
            outputStream.writeShort(keyMovements.size());
            for (Short s : keyMovements) {
                outputStream.writeShort(s);
            }
            outputStream.writeDouble(actorId);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
