package org.crohemu.network.d2protocol.message.messages.login;

import org.crohemu.network.d2protocol.D2DataOutputStream;
import org.crohemu.network.d2protocol.message.messages.D2MessageType;
import org.crohemu.network.d2protocol.message.messages.types.IdentificationFailedReason;

import java.io.IOException;

/**
 * Sent when a client tried to connect to a banned account.
 */
public class IdentificationFailedBannedMessage extends IdentificationFailedMessage {

    private double banEndDate;

    public IdentificationFailedBannedMessage(double banEndDate) {
        super(IdentificationFailedReason.BANNED);

        this.banEndDate = banEndDate;
    }

    @Override
    protected void serialize(D2DataOutputStream outputStream) {
        try {
            outputStream.writeByte(getReason());
            outputStream.writeDouble(banEndDate);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public double getBanEndDate() {
        return banEndDate;
    }

    public void setBanEndDate(double banEndDate) {
        this.banEndDate = banEndDate;
    }
}
