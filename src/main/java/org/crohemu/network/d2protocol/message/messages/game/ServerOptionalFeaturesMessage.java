package org.crohemu.network.d2protocol.message.messages.game;

import org.crohemu.network.d2protocol.D2DataOutputStream;
import org.crohemu.network.d2protocol.message.D2Message;
import org.crohemu.network.d2protocol.message.messages.D2MessageType;

import java.io.IOException;

/**
 * Created by A643558 on 08/12/2016.
 */
public class ServerOptionalFeaturesMessage extends D2Message {
    private byte[] features;

    public ServerOptionalFeaturesMessage(byte[] features) {
        this.features = features;

    }

    @Override
    protected void serialize(D2DataOutputStream outputStream) {
        try {
            outputStream.writeShort(features.length);
            for (byte feature: features) {
                outputStream.writeByte(feature);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
