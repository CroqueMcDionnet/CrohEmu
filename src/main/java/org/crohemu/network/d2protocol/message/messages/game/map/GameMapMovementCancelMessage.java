package org.crohemu.network.d2protocol.message.messages.game.map;

import org.crohemu.network.d2protocol.D2DataInputStream;
import org.crohemu.network.d2protocol.message.D2Message;

import java.io.IOException;

public class GameMapMovementCancelMessage extends D2Message {

    private short cellId;

    public GameMapMovementCancelMessage(byte[] messageData) {
        super(messageData);
    }

    @Override
    protected void deserialize(D2DataInputStream inputStream) {
        try {
            cellId = inputStream.readVariableLengthShort();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public short getCellId() {
        return cellId;
    }
}
