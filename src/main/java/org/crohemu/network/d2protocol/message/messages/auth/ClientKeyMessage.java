package org.crohemu.network.d2protocol.message.messages.auth;

import org.crohemu.network.d2protocol.D2DataInputStream;
import org.crohemu.network.d2protocol.message.D2Message;

import java.io.IOException;

public class ClientKeyMessage extends D2Message {

    private String key;
    public ClientKeyMessage(byte[] messageData) {
        super(messageData);
    }

    @Override
    protected void deserialize(D2DataInputStream inputStream) {
        try {
            key = inputStream.readUTF();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getKey() {
        return key;
    }
}
