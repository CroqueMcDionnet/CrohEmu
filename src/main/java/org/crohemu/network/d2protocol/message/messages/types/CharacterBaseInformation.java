package org.crohemu.network.d2protocol.message.messages.types;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.crohemu.network.d2protocol.D2DataInputStream;
import org.crohemu.network.d2protocol.D2DataOutputStream;
import org.crohemu.network.d2protocol.D2NetworkType;
import org.crohemu.network.d2protocol.message.messages.types.entity.EntityLook;

import java.io.IOException;

/**
 * Contains the information describing a character in a CharactersListMessage.
 */
public class CharacterBaseInformation implements D2NetworkType {

    /**
     * the client allows for multiple types of CharacterBaseInformation.
     * We're only using the one that contains the most features.
     */
    public static final int TYPE_ID = 45;

    private long id = 0;

    private String name = "";

    private int level = 1;

    private EntityLook entityLook = new EntityLook();

    private int breed = 0;

    private boolean sex = false;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public EntityLook getEntityLook() {
        return entityLook;
    }

    public void setEntityLook(EntityLook entityLook) {
        this.entityLook = entityLook;
    }

    public int getBreed() {
        return breed;
    }

    public void setBreed(int breed) {
        this.breed = breed;
    }

    public boolean isSex() {
        return sex;
    }

    public void setSex(boolean sex) {
        this.sex = sex;
    }

    @Override
    public void serialize(D2DataOutputStream outputStream) {
        try {
            outputStream.writeVariableLengthLong(id);
            outputStream.writeUTF(name);
            outputStream.writeByte(level);
            entityLook.serialize(outputStream);
            outputStream.writeByte(breed);
            outputStream.writeBoolean(sex);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deserialize(D2DataInputStream inputStream) {
        // only for testing purposes

        try {
            this.id = inputStream.readVariableLengthLong();
            this.name = inputStream.readUTF();
            this.level = inputStream.readByte();
            this.entityLook = new EntityLook();
            this.entityLook.deserialize(inputStream);
            this.breed = inputStream.readByte();
            this.sex = inputStream.readBoolean();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean isValid() {
        return id >= 0
                && level >= 1 && level <= 200
                && entityLook.isValid()
                && breed >= 1 && breed <= 17;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("name", name)
                .append("level", level)
                .append("entityLook", entityLook)
                .append("breed", breed)
                .append("sex", sex)
                .toString();
    }
}
