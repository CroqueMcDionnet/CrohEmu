package org.crohemu.network.d2protocol.message.messages.game.map;

import org.crohemu.network.d2protocol.message.D2Message;

public class GameMapMovementConfirmMessage extends D2Message {
    public GameMapMovementConfirmMessage(byte[] messageData) {
        super(messageData);
    }
}
