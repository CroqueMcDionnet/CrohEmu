package org.crohemu.network.d2protocol.message.messages.types.map;

import org.crohemu.network.d2protocol.D2DataInputStream;
import org.crohemu.network.d2protocol.D2DataOutputStream;
import org.crohemu.network.d2protocol.D2NetworkType;

import java.io.IOException;

public class MapObstacle implements D2NetworkType {

    private short obstacleCellId;
    private byte state;

    @Override
    public void serialize(D2DataOutputStream outputStream) {
        try {
            outputStream.writeVariableLengthShort(obstacleCellId);
            outputStream.writeByte(state);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deserialize(D2DataInputStream inputStream) {

    }

    @Override
    public boolean isValid() {
        return obstacleCellId >= 0 && obstacleCellId <= 559 && state >= 0;
    }

    public short getObstacleCellId() {
        return obstacleCellId;
    }

    public void setObstacleCellId(short obstacleCellId) {
        this.obstacleCellId = obstacleCellId;
    }

    public byte getState() {
        return state;
    }

    public void setState(byte state) {
        this.state = state;
    }
}
