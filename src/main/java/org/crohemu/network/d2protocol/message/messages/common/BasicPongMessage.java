package org.crohemu.network.d2protocol.message.messages.common;

import org.crohemu.network.d2protocol.D2DataOutputStream;
import org.crohemu.network.d2protocol.message.D2Message;
import org.crohemu.network.d2protocol.message.messages.D2MessageType;

import java.io.IOException;

/**
 * Reply to a BasicPingMessage
 */
public class BasicPongMessage extends D2Message {
    private boolean quiet;

    public BasicPongMessage(boolean quiet) {
        this.quiet = quiet;

    }

    @Override
    protected void serialize(D2DataOutputStream outputStream) {
        try {
            outputStream.writeBoolean(quiet);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
