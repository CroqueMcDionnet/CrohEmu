package org.crohemu.network.d2protocol.message.messages.login.character.creation;

import org.crohemu.network.d2protocol.D2DataOutputStream;
import org.crohemu.network.d2protocol.message.D2Message;
import org.crohemu.network.d2protocol.message.messages.D2MessageType;

import java.io.IOException;

public class CharacterCreationResultMessage extends D2Message {

    private ProtocolCharacterCreationResult result;

    public CharacterCreationResultMessage(ProtocolCharacterCreationResult result) {
        super();
        this.result = result;

    }

    @Override
    protected void serialize(D2DataOutputStream outputStream) {
        try {
            outputStream.writeByte(result.getValue());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ProtocolCharacterCreationResult getResult() {
        return result;
    }

    public void setResult(ProtocolCharacterCreationResult result) {
        this.result = result;
    }

    public enum ProtocolCharacterCreationResult {
        SUCCESS(0),
        FAILURE_NO_REASON(1),
        FAILURE_INVALID_NAME(2),
        FAILURE_NAME_ALREADY_TAKEN(3),
        FAILURE_TOO_MANY_CHARACTERS(4),
        FAILURE_NOT_ALLOWED(5),
        FAILURE_NEW_PLAYER_NOT_ALLOWED(6),
        FAILURE_RESTRICTED_ZONE(7);
        private final int value;
        ProtocolCharacterCreationResult(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
}
