package org.crohemu.network.d2protocol.message.factory.impl;

import org.crohemu.model.account.Account;
import org.crohemu.model.account.Subscription;
import org.crohemu.network.d2protocol.message.D2Message;
import org.crohemu.network.d2protocol.message.factory.D2MessageFactory;
import org.crohemu.network.d2protocol.message.messages.D2MessageType;
import org.crohemu.network.d2protocol.message.messages.login.*;
import org.crohemu.network.d2protocol.message.messages.types.GameServerInformation;
import org.crohemu.network.d2protocol.message.messages.types.GameServerInformation.GameServerStatus;
import org.crohemu.persistence.character.BreedRepository;
import org.crohemu.server.game.config.GameServerConfig;
import org.joda.time.Duration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Collections;

public class D2MessageFactoryImpl implements D2MessageFactory {

    Logger logger = LoggerFactory.getLogger(D2MessageFactoryImpl.class);

    @Inject
    BreedRepository breedRepository;

    @Override
    public D2Message createMessage(byte[] messageData) {

        int messageId = (messageData[0] << 6) | ((messageData[1] >> 2) & 0b00111111);

        for (D2MessageType type : D2MessageType.values()) {
            if (messageId == type.getId()) {
                try {
                    Constructor<? extends D2Message> constructor = type.getMessageClass().getConstructor(byte[].class);
                    return constructor.newInstance((Object) messageData);
                } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException | InstantiationException e) {
                    e.printStackTrace();
                }
                break;
            }
        }

        logger.error("Invalid packet id : {} \n" +
                "----- MESSAGE DUMP : {}", messageId, messageData);
        return null;
    }

    @Override
    public IdentificationSuccessMessage buildIdentificationSuccessMessage(Account account) {
        IdentificationSuccessMessage identificationSuccessMessage = new IdentificationSuccessMessage();
        Subscription accountSubscription = account.getSubscription();
        Duration accountSubscriptionElapsedDuration = new Duration(accountSubscription.getStartDate(),
                accountSubscription.getEndDate());

        identificationSuccessMessage.setSubscriptionElapsedDuration(accountSubscriptionElapsedDuration.getMillis());
        identificationSuccessMessage.setSubscriptionEndDate(accountSubscription.getEndDate().getMillis());
        identificationSuccessMessage.setAccountCreation(account.getCreationDate().getMillis());
        identificationSuccessMessage.setAccountId(account.getAccountId());
        identificationSuccessMessage.setCommunityId(account.getCommunity().getCommunityId());
        identificationSuccessMessage.setHasRights(account.getRights() >= Account.AccountRights.ADMIN.getId());
        identificationSuccessMessage.setHavenbagAvailableRoom(account.getHavenbagAvailableRoom());
        identificationSuccessMessage.setLogin(account.getAccountName());
        identificationSuccessMessage.setNickname(account.getNickname());
        identificationSuccessMessage.setSecretQuestion(account.getSecretQuestion());
        return identificationSuccessMessage;
    }

    @Override
    public SelectedServerDataMessage buildSelectedServerDataMessage(Account account) {
        SelectedServerDataMessage message = new SelectedServerDataMessage();

        message.setCanCreateNewCharacter(account.getCharacters().size() < GameServerConfig.getCharactersSlots());
        message.setAddress(GameServerConfig.getIpAddress());
        message.setPort(GameServerConfig.getPortNumber());
        message.setServerId(GameServerConfig.getServerId());
        message.setTicket(SelectedServerDataMessage.randomTicket());
        return message;
    }

    @Override
    public ServerSettingsMessage buildServerSettingsMessage() {
        ServerSettingsMessage message = new ServerSettingsMessage();

        message.setArenaLeaveBanTime(GameServerConfig.getArenaLeaveBanTime());
        message.setCommunity(GameServerConfig.getCommunity());
        message.setGameType(GameServerConfig.getType());
        message.setLang(GameServerConfig.getLang());

        return message;
    }

    @Override
    public ServersListMessage buildServersListMessage(Account account) {
        ServersListMessage serversListMessage = new ServersListMessage();
        serversListMessage.setAlreadyConnectedToServerId(-1);
        serversListMessage.setCanCreateNewCharacter(account.getCharacters().size() < GameServerConfig.getCharactersSlots());

        GameServerInformation gameServerInformation = new GameServerInformation();
        gameServerInformation.setCompletion(GameServerConfig.getCompletion());
        gameServerInformation.setStatus(GameServerStatus.ONLINE.getCode());
        gameServerInformation.setSelectable(true);
        gameServerInformation.setId(GameServerConfig.getServerId());
        gameServerInformation.setDate(GameServerConfig.getDate());
        gameServerInformation.setType(GameServerConfig.getType());
        gameServerInformation.setCharactersSlots(GameServerConfig.getCharactersSlots());
        gameServerInformation.setCharactersCount(account.getCharacters().size());

        serversListMessage.setServers(Collections.singletonList(gameServerInformation));

        return serversListMessage;
    }

    @Override
    public AccountCapabilitiesMessage buildAccountCapabilitiesMessage(Account account) {
        AccountCapabilitiesMessage message = new AccountCapabilitiesMessage();
        message.setStatus(1);
        message.setCanCreateNewCharacter(account.getCharacters().size() < GameServerConfig.getCharactersSlots());
        message.setAccountId(account.getAccountId());
        message.setBreedsAvailable(0b1111111111111111);
        message.setBreedsVisible(0b1111111111111111);
        message.setTutorialAvailable(true);

        return message;
    }
}
