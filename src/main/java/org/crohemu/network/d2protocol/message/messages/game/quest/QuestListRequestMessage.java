package org.crohemu.network.d2protocol.message.messages.game.quest;

import org.crohemu.network.d2protocol.message.D2Message;

public class QuestListRequestMessage extends D2Message {
    public QuestListRequestMessage(byte[] messageData) {
        super(messageData);
    }
}
