package org.crohemu.network.d2protocol.message.messages.game.map;

import org.crohemu.network.d2protocol.D2DataOutputStream;
import org.crohemu.network.d2protocol.message.D2Message;
import org.crohemu.network.d2protocol.message.messages.types.actor.GameRolePlayActorInformation;

import java.io.IOException;

public class GameRolePlayShowActorMessage extends D2Message {

    private GameRolePlayActorInformation information;

    public GameRolePlayShowActorMessage(GameRolePlayActorInformation information) {
        this.information = information;
    }

    @Override
    protected void serialize(D2DataOutputStream outputStream) {
        try {
            outputStream.writeShort(information.getTypeId());
            information.serialize(outputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public GameRolePlayActorInformation getInformation() {
        return information;
    }

    public void setInformation(GameRolePlayActorInformation information) {
        this.information = information;
    }
}
