package org.crohemu.network.d2protocol.message.factory;

import org.crohemu.model.account.Account;
import org.crohemu.network.d2protocol.message.D2Message;
import org.crohemu.network.d2protocol.message.messages.login.ServerSettingsMessage;
import org.crohemu.network.d2protocol.message.messages.login.AccountCapabilitiesMessage;
import org.crohemu.network.d2protocol.message.messages.login.IdentificationSuccessMessage;
import org.crohemu.network.d2protocol.message.messages.login.SelectedServerDataMessage;
import org.crohemu.network.d2protocol.message.messages.login.ServersListMessage;

/**
 * Factory design pattern applied to D2Message
 */
public interface D2MessageFactory {

    /**
     * Creates a message from raw socket data. D2MessageFactory will take care of instantiating
     * the correct message type.
     * @param messageData Raw socket data
     * @return A new deserialized D2Message.
     */
    D2Message createMessage(byte[] messageData);

    /**
     * Builds an IdentificationSuccessMessage for a specific account.
     * @param account The account that just successfully identified.
     * @return An IdentificationSuccessMessage ready to be sent.
     */
    IdentificationSuccessMessage buildIdentificationSuccessMessage(Account account);

    SelectedServerDataMessage buildSelectedServerDataMessage(Account account);

    ServerSettingsMessage buildServerSettingsMessage();

    ServersListMessage buildServersListMessage(Account account);

    AccountCapabilitiesMessage buildAccountCapabilitiesMessage(Account account);
}
