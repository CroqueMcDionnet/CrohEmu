package org.crohemu.network.d2protocol.message.messages.misc;

import org.crohemu.network.d2protocol.D2DataOutputStream;
import org.crohemu.network.d2protocol.message.D2Message;
import org.crohemu.network.d2protocol.message.messages.D2MessageType;

import java.io.IOException;

/**
 * Sent to the client on game server connection to synchronize its time.
 */
public class BasicTimeMessage extends D2Message {
    long timestamp;
    int timezoneOffset;

    public BasicTimeMessage(long timestamp, int timezoneOffset) {
        this.timestamp = timestamp;
        this.timezoneOffset = timezoneOffset;


    }

    @Override
    public void serialize(D2DataOutputStream outputStream) {
        try {
            outputStream.writeDouble(timestamp);
            outputStream.writeShort(timezoneOffset);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public void setTimezoneOffset(int timezoneOffset) {
        this.timezoneOffset = timezoneOffset;
    }
}
