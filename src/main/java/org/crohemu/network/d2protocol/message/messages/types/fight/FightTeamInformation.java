package org.crohemu.network.d2protocol.message.messages.types.fight;

import org.crohemu.network.d2protocol.D2DataOutputStream;

import java.io.IOException;
import java.util.List;

public class FightTeamInformation extends AbstractFightTeamInformation {

    private List<FightTeamMemberInformation> teamMembers;

    @Override
    public void serialize(D2DataOutputStream outputStream) {
        super.serialize(outputStream);
        try {
            outputStream.writeShort(teamMembers.size());
            for (FightTeamMemberInformation information : teamMembers) {
                outputStream.writeShort(information.getTypeId());
                information.serialize(outputStream);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getTypeId() {
        return 33;
    }
}
