package org.crohemu.network.d2protocol.message.messages.types.actor;

import org.crohemu.network.d2protocol.D2DataOutputStream;
import org.crohemu.network.d2protocol.message.messages.types.entity.HumanoidInfo;

import java.io.IOException;

public class GameRolePlayHumanoidInformation extends GameRolePlayNamedActorInformation {

    private HumanoidInfo humanoidInfo;
    private int accountId;

    @Override
    public int getTypeId() {
        return 159;
    }

    @Override
    public void serialize(D2DataOutputStream outputStream) {
        super.serialize(outputStream);
        try {
            outputStream.writeShort(humanoidInfo.getTypeId());
            humanoidInfo.serialize(outputStream);
            outputStream.writeInt(accountId);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public HumanoidInfo getHumanoidInfo() {
        return humanoidInfo;
    }

    public void setHumanoidInfo(HumanoidInfo humanoidInfo) {
        this.humanoidInfo = humanoidInfo;
    }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }
}
