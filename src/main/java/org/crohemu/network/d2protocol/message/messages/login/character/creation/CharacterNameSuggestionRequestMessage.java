package org.crohemu.network.d2protocol.message.messages.login.character.creation;

import org.crohemu.network.d2protocol.message.D2Message;
import org.crohemu.network.d2protocol.message.messages.D2MessageType;
import org.crohemu.network.tcpwrapper.TcpClient;

public class CharacterNameSuggestionRequestMessage extends D2Message {
    public CharacterNameSuggestionRequestMessage(byte[] messageData) {
        super(messageData);

    }
}
