package org.crohemu.network.d2protocol.message.messages.game.map;

import org.crohemu.network.d2protocol.D2DataOutputStream;
import org.crohemu.network.d2protocol.message.D2Message;

import java.io.IOException;

public class CurrentMapMessage extends D2Message {

    private int mapId;
    private String mapsKey;

    public CurrentMapMessage(int mapId, String mapsKey) {
        this.mapId = mapId;
        this.mapsKey = mapsKey;
    }

    @Override
    protected void serialize(D2DataOutputStream outputStream) {
        try {
            outputStream.writeInt(mapId);
            outputStream.writeUTF(mapsKey);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public int getMapId() {
        return mapId;
    }

    public void setMapId(int mapId) {
        this.mapId = mapId;
    }

    public String getMapsKey() {
        return mapsKey;
    }

    public void setMapsKey(String mapsKey) {
        this.mapsKey = mapsKey;
    }
}
