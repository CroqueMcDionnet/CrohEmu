package org.crohemu.network.d2protocol.message.messages.login.character;

import org.crohemu.network.d2protocol.message.D2Message;
import org.crohemu.network.d2protocol.message.messages.D2MessageType;
import org.crohemu.network.tcpwrapper.TcpClient;

public class CharactersListRequestMessage extends D2Message {
    public CharactersListRequestMessage(byte[] messageData) {
        super(messageData);

    }
}
