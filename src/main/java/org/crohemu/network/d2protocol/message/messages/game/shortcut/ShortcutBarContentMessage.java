package org.crohemu.network.d2protocol.message.messages.game.shortcut;

import org.crohemu.model.character.shortcut.Shortcut;
import org.crohemu.network.d2protocol.D2DataOutputStream;
import org.crohemu.network.d2protocol.message.D2Message;

import java.io.IOException;
import java.util.List;

public class ShortcutBarContentMessage extends D2Message {

    public final static byte TYPE_GENERAL_SHORTCUT_BAR = 0;
    public final static byte TYPE_SPELL_SHORTCUT_BAR = 1;

    private byte barType;
    private List<? extends Shortcut> shortcuts;

    public ShortcutBarContentMessage(byte barType, List<? extends Shortcut> shortcuts) {
        this();
        this.barType = barType;
        this.shortcuts = shortcuts;
    }

    public ShortcutBarContentMessage() {

    }

    @Override
    protected void serialize(D2DataOutputStream outputStream) {
        try {
            outputStream.writeByte(barType);
            outputStream.writeShort(shortcuts.size());
            shortcuts.forEach(shortcut -> shortcut.serialize(outputStream));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public byte getBarType() {
        return barType;
    }

    public void setBarType(byte barType) {
        this.barType = barType;
    }

    public List<? extends Shortcut> getShortcuts() {
        return shortcuts;
    }

    public void setShortcuts(List<Shortcut> shortcuts) {
        this.shortcuts = shortcuts;
    }
}
