package org.crohemu.network.d2protocol.message.messages.types.entity;

import org.crohemu.network.d2protocol.D2DataInputStream;
import org.crohemu.network.d2protocol.D2DataOutputStream;
import org.crohemu.network.d2protocol.D2NetworkType;
import org.crohemu.network.d2protocol.message.messages.types.actor.ActorRestrictionInformation;

import java.io.IOException;
import java.util.List;

public class HumanoidInfo implements D2NetworkType {

    private ActorRestrictionInformation restrictions;
    private boolean sex;
    private List<HumanOption> options;

    public int getTypeId() {
        return 157;
    }

    @Override
    public void serialize(D2DataOutputStream outputStream) {
        restrictions.serialize(outputStream);
        try {
            outputStream.writeBoolean(sex);
            outputStream.writeShort(options.size());
            for (HumanOption option : options) {
                outputStream.writeShort(option.getTypeId());
                option.serialize(outputStream);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deserialize(D2DataInputStream inputStream) {
        // unused
    }

    @Override
    public boolean isValid() {
        return true;
    }

    public ActorRestrictionInformation getRestrictions() {
        return restrictions;
    }

    public void setRestrictions(ActorRestrictionInformation restrictions) {
        this.restrictions = restrictions;
    }

    public boolean isSex() {
        return sex;
    }

    public void setSex(boolean sex) {
        this.sex = sex;
    }

    public List<HumanOption> getOptions() {
        return options;
    }

    public void setOptions(List<HumanOption> options) {
        this.options = options;
    }
}
