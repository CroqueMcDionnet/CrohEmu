package org.crohemu.network.d2protocol.message.messages.login;

import org.crohemu.network.d2protocol.D2DataOutputStream;
import org.crohemu.network.d2protocol.message.D2Message;
import org.crohemu.network.d2protocol.utils.BooleanFlagsWriter;

import java.io.IOException;

public class AccountCapabilitiesMessage extends D2Message {

    private int accountId;
    private int breedsVisible;
    private int breedsAvailable;
    private int status;

    private boolean tutorialAvailable;
    private boolean canCreateNewCharacter;

    public AccountCapabilitiesMessage() {

    }

    @Override
    protected void serialize(D2DataOutputStream outputStream) {
        try {
            outputStream.writeByte(BooleanFlagsWriter.flagsToByte(tutorialAvailable, canCreateNewCharacter));
            outputStream.writeInt(accountId);
            outputStream.writeVariableLengthInt(breedsVisible);
            outputStream.writeVariableLengthInt(breedsAvailable);
            outputStream.writeByte(status);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public int getBreedsVisible() {
        return breedsVisible;
    }

    public void setBreedsVisible(int breedsVisible) {
        this.breedsVisible = breedsVisible;
    }

    public int getBreedsAvailable() {
        return breedsAvailable;
    }

    public void setBreedsAvailable(int breedsAvailable) {
        this.breedsAvailable = breedsAvailable;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public boolean isTutorialAvailable() {
        return tutorialAvailable;
    }

    public void setTutorialAvailable(boolean tutorialAvailable) {
        this.tutorialAvailable = tutorialAvailable;
    }

    public boolean isCanCreateNewCharacter() {
        return canCreateNewCharacter;
    }

    public void setCanCreateNewCharacter(boolean canCreateNewCharacter) {
        this.canCreateNewCharacter = canCreateNewCharacter;
    }
}
