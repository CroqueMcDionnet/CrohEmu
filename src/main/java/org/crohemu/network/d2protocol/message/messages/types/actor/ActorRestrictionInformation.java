package org.crohemu.network.d2protocol.message.messages.types.actor;

import org.crohemu.network.d2protocol.D2DataInputStream;
import org.crohemu.network.d2protocol.D2DataOutputStream;
import org.crohemu.network.d2protocol.D2NetworkType;
import org.crohemu.network.d2protocol.utils.BooleanFlagsWriter;

import java.io.IOException;

public class ActorRestrictionInformation implements D2NetworkType {
    
    private boolean cantBeAggressed = false;

    private boolean cantBeChallenged = false;

    private boolean cantTrade = false;

    private boolean cantBeAttackedByMutant = false;

    private boolean cantRun = false;

    private boolean forceSlowWalk = false;

    private boolean cantMinimize = false;

    private boolean cantMove = false;

    private boolean cantAggress = false;

    private boolean cantChallenge = false;

    private boolean cantExchange = false;

    private boolean cantAttack = false;

    private boolean cantChat = false;

    private boolean cantBeMerchant = false;

    private boolean cantUseObject = false;

    private boolean cantUseTaxCollector = false;

    private boolean cantUseInteractive = false;

    private boolean cantSpeakToNPC = false;

    private boolean cantChangeZone = false;

    private boolean cantAttackMonster = false;

    private boolean cantWalk8Directions = false;

    @Override
    public void serialize(D2DataOutputStream outputStream) {
        try {
            outputStream.writeByte(BooleanFlagsWriter.flagsToByte(
                    cantBeAggressed,
                    cantBeChallenged,
                    cantTrade,
                    cantBeAttackedByMutant,
                    cantRun,
                    forceSlowWalk,
                    cantMinimize,
                    cantMove
            ));
            outputStream.writeByte(BooleanFlagsWriter.flagsToByte(
                    cantAggress,
                    cantChallenge,
                    cantExchange,
                    cantAttack,
                    cantChat,
                    cantBeMerchant,
                    cantUseObject,
                    cantUseTaxCollector
            ));
            outputStream.writeByte(BooleanFlagsWriter.flagsToByte(
                    cantUseInteractive,
                    cantSpeakToNPC,
                    cantChangeZone,
                    cantAttackMonster,
                    cantWalk8Directions
            ));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deserialize(D2DataInputStream inputStream) {
        // unused
    }

    @Override
    public boolean isValid() {
        return true;
    }

    public boolean isCantBeAggressed() {
        return cantBeAggressed;
    }

    public void setCantBeAggressed(boolean cantBeAggressed) {
        this.cantBeAggressed = cantBeAggressed;
    }

    public boolean isCantBeChallenged() {
        return cantBeChallenged;
    }

    public void setCantBeChallenged(boolean cantBeChallenged) {
        this.cantBeChallenged = cantBeChallenged;
    }

    public boolean isCantTrade() {
        return cantTrade;
    }

    public void setCantTrade(boolean cantTrade) {
        this.cantTrade = cantTrade;
    }

    public boolean isCantBeAttackedByMutant() {
        return cantBeAttackedByMutant;
    }

    public void setCantBeAttackedByMutant(boolean cantBeAttackedByMutant) {
        this.cantBeAttackedByMutant = cantBeAttackedByMutant;
    }

    public boolean isCantRun() {
        return cantRun;
    }

    public void setCantRun(boolean cantRun) {
        this.cantRun = cantRun;
    }

    public boolean isForceSlowWalk() {
        return forceSlowWalk;
    }

    public void setForceSlowWalk(boolean forceSlowWalk) {
        this.forceSlowWalk = forceSlowWalk;
    }

    public boolean isCantMinimize() {
        return cantMinimize;
    }

    public void setCantMinimize(boolean cantMinimize) {
        this.cantMinimize = cantMinimize;
    }

    public boolean isCantMove() {
        return cantMove;
    }

    public void setCantMove(boolean cantMove) {
        this.cantMove = cantMove;
    }

    public boolean isCantAggress() {
        return cantAggress;
    }

    public void setCantAggress(boolean cantAggress) {
        this.cantAggress = cantAggress;
    }

    public boolean isCantChallenge() {
        return cantChallenge;
    }

    public void setCantChallenge(boolean cantChallenge) {
        this.cantChallenge = cantChallenge;
    }

    public boolean isCantExchange() {
        return cantExchange;
    }

    public void setCantExchange(boolean cantExchange) {
        this.cantExchange = cantExchange;
    }

    public boolean isCantAttack() {
        return cantAttack;
    }

    public void setCantAttack(boolean cantAttack) {
        this.cantAttack = cantAttack;
    }

    public boolean isCantChat() {
        return cantChat;
    }

    public void setCantChat(boolean cantChat) {
        this.cantChat = cantChat;
    }

    public boolean isCantBeMerchant() {
        return cantBeMerchant;
    }

    public void setCantBeMerchant(boolean cantBeMerchant) {
        this.cantBeMerchant = cantBeMerchant;
    }

    public boolean isCantUseObject() {
        return cantUseObject;
    }

    public void setCantUseObject(boolean cantUseObject) {
        this.cantUseObject = cantUseObject;
    }

    public boolean isCantUseTaxCollector() {
        return cantUseTaxCollector;
    }

    public void setCantUseTaxCollector(boolean cantUseTaxCollector) {
        this.cantUseTaxCollector = cantUseTaxCollector;
    }

    public boolean isCantUseInteractive() {
        return cantUseInteractive;
    }

    public void setCantUseInteractive(boolean cantUseInteractive) {
        this.cantUseInteractive = cantUseInteractive;
    }

    public boolean isCantSpeakToNPC() {
        return cantSpeakToNPC;
    }

    public void setCantSpeakToNPC(boolean cantSpeakToNPC) {
        this.cantSpeakToNPC = cantSpeakToNPC;
    }

    public boolean isCantChangeZone() {
        return cantChangeZone;
    }

    public void setCantChangeZone(boolean cantChangeZone) {
        this.cantChangeZone = cantChangeZone;
    }

    public boolean isCantAttackMonster() {
        return cantAttackMonster;
    }

    public void setCantAttackMonster(boolean cantAttackMonster) {
        this.cantAttackMonster = cantAttackMonster;
    }

    public boolean isCantWalk8Directions() {
        return cantWalk8Directions;
    }

    public void setCantWalk8Directions(boolean cantWalk8Directions) {
        this.cantWalk8Directions = cantWalk8Directions;
    }
}
