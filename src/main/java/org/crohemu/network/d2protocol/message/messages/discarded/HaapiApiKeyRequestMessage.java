package org.crohemu.network.d2protocol.message.messages.discarded;

import org.crohemu.network.d2protocol.D2DataInputStream;
import org.crohemu.network.d2protocol.message.D2Message;

import java.io.IOException;

public class HaapiApiKeyRequestMessage extends D2Message {

    private int keyType;

    public HaapiApiKeyRequestMessage(byte[] messageData) {
        super(messageData);
    }

    @Override
    protected void deserialize(D2DataInputStream inputStream) {
        try {
            this.keyType = inputStream.readByte();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public int getKeyType() {
        return keyType;
    }
}
