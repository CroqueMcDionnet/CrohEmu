package org.crohemu.network.d2protocol.server;

import org.crohemu.model.account.Account;
import org.crohemu.network.d2protocol.D2Client;

import java.io.IOException;
import java.net.InetAddress;
import java.util.Set;

public interface D2Server {

    Set<D2Client> getD2Clients();

    Type getType();

    void start(InetAddress ipAddress, int portNumber) throws IOException;

    void disconnectClient(D2Client client);

    Set<D2Client> getClientsConnectedOnAccount(Account account);

    enum Type { LOGIN, BOTH, GAME }
}
