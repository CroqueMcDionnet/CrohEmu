package org.crohemu.network.d2protocol;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * DataOutputStream encapsulating a ByteArrayOutputStream with additional functionnality
 * specific to the D2 protocol.
 */
public class D2DataOutputStream extends DataOutputStream {

    private static final int BYTE_NEGATIVE_LIMIT = 0b10000000;
    private static final int CHUNK_BIT_SIZE = 7;
    private static final int LOWEST_BYTE_MASK = 0b11111111;

    private ByteArrayOutputStream underlyingStream;

    public D2DataOutputStream() {
        this(new ByteArrayOutputStream());
    }

    private D2DataOutputStream(OutputStream out) {
        super(out);
        this.underlyingStream = (ByteArrayOutputStream) out;
    }

    /**
     * Writes an int of variable length. This is specific to the D2 protocol.
     * @param i
     * @throws IOException
     */
    public void writeVariableLengthInt(int i) throws IOException {
        if (i == 0) {
            this.writeByte(0);
            return;
        }

        while (i != 0) {
            int lastByte = (i & LOWEST_BYTE_MASK);

            i = i >>> CHUNK_BIT_SIZE;

            this.write(i > 0
                    ? (byte) (lastByte | BYTE_NEGATIVE_LIMIT)
                    : (byte) lastByte);
        }
    }

    /**
     * Writes a short of variable length. This is specific to the D2 protocol.
     * @param i
     * @throws IOException
     */
    public void writeVariableLengthShort(int i) throws IOException {
        if (i > Short.MAX_VALUE || i < Short.MIN_VALUE) {
            throw new IllegalArgumentException("input is out of bounds");
        }
        if (i >= 0 && i < 0b01111111) {
            writeByte(i);
            return;
        }
        i = i & 65535;

        while (i != 0) {
            int lastByte = (i & LOWEST_BYTE_MASK);

            i = i >>> CHUNK_BIT_SIZE;
            this.write(i > 0
                    ? (byte) (lastByte | BYTE_NEGATIVE_LIMIT)
                    : (byte) lastByte);
        }
    }

    /**
     * Writes a long of variable length. This is specific to the D2 protocol.
     * @param l
     * @throws IOException
     */
    public void writeVariableLengthLong(long l)  throws IOException {
        int high = (int) (l >>> Integer.SIZE);
        int low = (int) ((l << Integer.SIZE) >>> Integer.SIZE);

        if (high == 0) {
            this.writeInt32(low);
            return;
        }

        for (int byteNumber = 0; byteNumber < Integer.BYTES; byteNumber++) {
            this.write(low & 127 | 128);
            low = low >>> 7;
        }
        if ((high & 268435455 << 3) == 0) {
            this.write(high << 4 | low);
        } else {
            this.write((high << 4 | low) & 127 | 128);
            this.writeInt32(high >>> 3);
        }
    }

    private void writeInt32(int i) throws IOException {
        while (i >= 128) {
            this.writeByte(i & 127 | 128);
            i = i >>> 7;
        }
        this.writeByte(i);
    }

    public ByteArrayOutputStream getUnderlyingStream() {
        return underlyingStream;
    }
}
