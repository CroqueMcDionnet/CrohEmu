package org.crohemu.network.d2protocol;

import org.crohemu.model.account.Account;
import org.crohemu.model.character.Character;
import org.crohemu.network.d2protocol.message.D2Message;
import org.crohemu.network.tcpwrapper.TcpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class D2Client {

    private final Logger logger = LoggerFactory.getLogger(D2Client.class);

    private final TcpClient tcpClient;
    private boolean identified = false;

    private Account account = null;
    private Character currentCharacter = null;
    private int[] authTicket;
    private boolean authenticated;

    public D2Client(TcpClient tcpClient) {
        this.tcpClient = tcpClient;
    }

    public TcpClient getTcpClient() {
        return tcpClient;
    }

    public void setIdentified(boolean identified) {
        this.identified = identified;
    }

    public boolean isIdentified() {
        return identified;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Account getAccount() {
        return account;
    }

    public void setAuthTicket(int[] authTicket) {
        this.authTicket = authTicket;
    }

    public int[] getAuthTicket() {
        return authTicket;
    }

    public void setAuthenticated(boolean authenticated) {
        this.authenticated = authenticated;
    }

    public boolean isAuthenticated() {
        return authenticated;
    }

    public void sendMessage(D2Message message) {
        try {
            tcpClient.sendMessage(message);
        } catch (IOException e) {
            logger.error("Could not send message {} to client {}", message, this);
        }
    }

    @Override
    public String toString() {
        return tcpClient.toString();
    }

    public Character getCurrentCharacter() {
        return currentCharacter;
    }

    public void setCurrentCharacter(Character currentCharacter) {
        this.currentCharacter = currentCharacter;
    }
}
