package org.crohemu.network.d2protocol;

import java.io.*;

/**
 * A DataInputStream encapsulating a ByteArrayInputStream with other methods specific to the D2 protocol?
 */
public class D2DataInputStream extends DataInputStream {

    private static final int BYTE_NEGATIVE_LIMIT = 0b10000000;
    private static final int BYTE_POSITIVE_LIMIT = 0b01111111;
    private static final int CHUNK_BIT_SIZE = 7;
    private static final int USHORT_MAX_VALUE = 65535;

    private ByteArrayInputStream underlyingStream;

    public D2DataInputStream(byte[] buffer) {
        this(new ByteArrayInputStream(buffer));
    }

    /**
     * @see ByteArrayInputStream#ByteArrayInputStream(byte[], int, int)
     */
    public D2DataInputStream(byte[] buffer, int offset, int length) {
        this(new ByteArrayInputStream(buffer, offset, length));
    }
    private D2DataInputStream(InputStream in) {
        super(in);
        this.underlyingStream = (ByteArrayInputStream) in;
    }

    /**
     * Reads an int of variable length. This is specific to the D2 protocol.
     * @return
     * @throws IOException
     */
    public int readVariableLengthInt() throws IOException {

        int result = 0;
        int bitsRead = 0;
        while (bitsRead < Integer.SIZE) {
            int latestByteRead = readByte();
            result = result + ((latestByteRead & BYTE_POSITIVE_LIMIT) << bitsRead);
            bitsRead = bitsRead + CHUNK_BIT_SIZE;

            if ((latestByteRead & BYTE_NEGATIVE_LIMIT) != BYTE_NEGATIVE_LIMIT) {
                return result;
            }
        }
        throw new IOException("Too much data");
    }

    /**
     * Reads a short of variable length. This is specific to the D2 protocol.
     * @return
     * @throws IOException
     */
    public short readVariableLengthShort() throws IOException {
        int result = 0;
        int bitsRead = 0;
        while (bitsRead < Short.SIZE) {
            int latestByteRead = readByte();
            result = result + ((latestByteRead & BYTE_POSITIVE_LIMIT) << bitsRead);
            bitsRead = bitsRead + CHUNK_BIT_SIZE;

            if ((latestByteRead & BYTE_NEGATIVE_LIMIT) != BYTE_NEGATIVE_LIMIT) {
                if (result > Short.MAX_VALUE) {
                    result = result - USHORT_MAX_VALUE;
                }
                return (short) result;
            }
        }
        throw new IOException("Too much data");
    }

    /**
     * Reads a long of variable length. This is specific to the D2 protocol.
     * @return
     * @throws IOException
     */
    public long readVariableLengthLong() throws IOException {
        int bitsRead = 0;
        int resultHigh = 0;
        int resultLow = 0;
        int lastByte = 0;
        while (true) {
            lastByte = readUnsignedByte();
            if (bitsRead == 28) {
                break;
            }
            if (lastByte >= 128) {
                resultLow = resultLow | (lastByte & 127) << bitsRead;
                bitsRead += 7;
                continue;
            }
            resultLow = resultLow | lastByte << bitsRead;
            return ((long) resultHigh << 32) | resultLow;
        }
        if (lastByte >= 128) {
            lastByte = lastByte & 127;
            resultLow = resultLow | lastByte << bitsRead;
            resultHigh = lastByte >>> 4;
            bitsRead = 3;

            while (true) {
                lastByte = readUnsignedByte();
                if (bitsRead < 32) {
                    if (lastByte >= 128) {
                        resultHigh = resultHigh | (lastByte & 127) << bitsRead;
                    } else {
                        break;
                    }
                }
                bitsRead += 7;
            }

            resultHigh = resultHigh | lastByte << bitsRead;
            return ((long) resultHigh << 32) | ((long) resultLow & 4294967295L);
        }
        resultLow = resultLow | lastByte << bitsRead;
        resultHigh = lastByte >>> 4;
        return ((long) resultHigh << 32) | ((long) resultLow & 4294967295L);
    }

    public ByteArrayInputStream getUnderlyingStream() {
        return underlyingStream;
    }
}
