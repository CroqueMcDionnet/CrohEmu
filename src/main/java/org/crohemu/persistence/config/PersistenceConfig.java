package org.crohemu.persistence.config;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.support.PropertiesLoaderUtils;

import java.io.IOException;
import java.util.Properties;

public class PersistenceConfig {
    private static Logger logger = LoggerFactory.getLogger(PersistenceConfig.class);
    private static final String CONFIG_FILE_PATH = "./config/persistence.properties";

    private static Properties configProperties;
    private static String databaseHost;
    private static int databasePortNumber;
    private static String databaseName;
    private static String databaseUsername;
    private static String databasePassword;

    // Loads the config file then reads it
    static {
        try {
            configProperties = PropertiesLoaderUtils.loadProperties(new FileSystemResource(CONFIG_FILE_PATH));
        } catch (IOException e) {
            e.printStackTrace();
        }

        readProperties();
    }

    private static void readProperties() {
        databaseHost = readDatabaseHost();
        databasePortNumber = readDatabasePortNumber();
        databaseName = readDatabaseName();
        databaseUsername = readDatabaseUsername();
        databasePassword = readDatabasePassword();
    }

    private static String readDatabasePassword() {
        final String PROP_NAME = "databasePassword";

        String databasePassword = configProperties.getProperty(PROP_NAME);

        if (StringUtils.isBlank(databasePassword)) {
            logger.warn("{} is empty. Is this right ? ({})", PROP_NAME, CONFIG_FILE_PATH);
        }

        return databasePassword;
    }

    private static String readDatabaseUsername() {
        final String PROP_NAME = "databaseUsername";

        String databaseUsername = configProperties.getProperty(PROP_NAME);

        if (StringUtils.isBlank(databaseUsername)) {
            logger.warn("{} is empty. Is this right ? ({})", PROP_NAME, CONFIG_FILE_PATH);
        }

        return databaseUsername;
    }

    private static String readDatabaseName() {
        final String PROP_NAME = "databaseName";

        String databaseName = configProperties.getProperty(PROP_NAME);

        if (StringUtils.isBlank(databaseName)) {
            logger.error("{} is empty. This is not right. ({})", PROP_NAME, CONFIG_FILE_PATH);
        }

        return databaseName;
    }

    private static int readDatabasePortNumber() {
        final String PROP_NAME = "databasePortNumber";

        int databasePortNumber = Integer.parseInt(configProperties.getProperty(PROP_NAME));

        if (databasePortNumber < 1024) {
            logger.warn("{} < 1024. Is this right ? ({})", PROP_NAME, CONFIG_FILE_PATH);
        } else if (databasePortNumber > 65535) {
            logger.error("{} can't be greater than 65535. ({})", PROP_NAME, CONFIG_FILE_PATH);
        }

        return databasePortNumber;
    }

    private static String readDatabaseHost() {
        final String PROP_NAME = "databaseHost";

        String databaseHost = configProperties.getProperty(PROP_NAME);

        if (StringUtils.isBlank(databaseHost)) {
            logger.warn("{} is empty. Is this right ? ({})", PROP_NAME, CONFIG_FILE_PATH);
        }

        return databaseHost;
    }

    public static String getDatabaseHost() {
        return databaseHost;
    }

    public static int getDatabasePortNumber() {
        return databasePortNumber;
    }

    public static String getDatabaseName() {
        return databaseName;
    }

    public static String getDatabaseUsername() {
        return databaseUsername;
    }

    public static String getDatabasePassword() {
        return databasePassword;
    }
}
