package org.crohemu.persistence.account;

import org.crohemu.model.account.Account;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface AccountRepository extends MongoRepository<Account, String>{
    Account findByAccountNameAndPassword(String accountName, String password);

    Account findByCharactersName(String name);

    Account findByCharactersCharacterId(long characterId);

    Set<Account> findByCharactersCurrentMapId(int mapId);
}
