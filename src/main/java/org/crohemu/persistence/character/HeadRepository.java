package org.crohemu.persistence.character;

import org.crohemu.model.character.Head;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HeadRepository extends MongoRepository<Head, String> {

    Head findById(int id);
}
