package org.crohemu.persistence.character;

import org.crohemu.model.character.Breed;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BreedRepository extends MongoRepository<Breed, String> {

    Breed findByBreedId(int breedId);
}
