package org.crohemu.persistence.world.map;

import org.crohemu.model.world.GameMap;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface GameMapRepository extends MongoRepository<GameMap, String> {

    GameMap findById(int mapId);
}
