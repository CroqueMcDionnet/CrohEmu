package org.crohemu.persistence.world;

import org.crohemu.model.world.MapReference;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface MapReferenceRepository extends MongoRepository<MapReference, String> {
}
