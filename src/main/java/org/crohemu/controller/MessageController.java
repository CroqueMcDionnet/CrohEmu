package org.crohemu.controller;

import org.crohemu.network.d2protocol.D2Client;
import org.crohemu.network.d2protocol.message.D2Message;

/**
 * Functional interface that allows routing a message to a behavior.
 */
@FunctionalInterface
public interface MessageController {
    /**
     * Executes the behavior for message.
     * @param message
     */
    void handleMessage(D2Message message, D2Client source);
}
