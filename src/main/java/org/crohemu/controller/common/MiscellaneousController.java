package org.crohemu.controller.common;

import org.crohemu.controller.D2Controller;
import org.crohemu.controller.D2MessageReceiver;
import org.crohemu.network.d2protocol.D2Client;
import org.crohemu.network.d2protocol.message.messages.D2MessageType;
import org.crohemu.network.d2protocol.message.messages.common.BasicAckMessage;
import org.crohemu.network.d2protocol.message.messages.common.BasicPingMessage;
import org.crohemu.network.d2protocol.message.messages.common.BasicPongMessage;
import org.crohemu.network.d2protocol.message.messages.discarded.HaapiApiKeyRequestMessage;
import org.crohemu.network.d2protocol.server.D2Server;

@D2Controller(serverType = D2Server.Type.BOTH)
public class MiscellaneousController {

    @D2MessageReceiver
    public void ping(BasicPingMessage message, D2Client source) {
        source.sendMessage(new BasicPongMessage(message.isQuiet()));
    }

    @D2MessageReceiver
    public void requestHaapiApiKey(HaapiApiKeyRequestMessage message, D2Client source) {
        source.sendMessage(new BasicAckMessage(0, D2MessageType.HAAPI_API_KEY_REQUEST.getId()));
    }
}
