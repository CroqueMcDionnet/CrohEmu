package org.crohemu.controller.admin;

import org.crohemu.ServerConsole;
import org.crohemu.admin.AdminCommand;
import org.crohemu.admin.TeleportPlayerCommand;
import org.crohemu.network.d2protocol.D2Client;
import org.crohemu.server.game.GameServer;
import org.crohemu.service.account.CharacterService;
import org.crohemu.service.world.GameMapService;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.Optional;

@Component
public class AdminCommandController {

    @Inject
    ServerConsole serverConsole;

    @Inject
    GameMapService gameMapService;

    @Inject
    CharacterService characterService;

    @Inject
    GameServer gameServer;

    public void dispatchCommand(AdminCommand adminCommand) {
        if (adminCommand instanceof TeleportPlayerCommand) {
            this.teleportPlayer((TeleportPlayerCommand) adminCommand);
        }
    }

    private void teleportPlayer(TeleportPlayerCommand command) {
        Optional<D2Client> client = gameServer.getD2Clients().stream()
                .filter(d2Client -> d2Client.getCurrentCharacter().getName().equals(command.getCharacterName()))
                .findAny();

        if (!client.isPresent()) {
            serverConsole.error("Cannot find character %s.", command.getCharacterName());
        } else {
            serverConsole.info("Teleporting character %s to map %d on cell %d", command.getCharacterName(),
                    command.getMapId(), command.getCellId());
            //todo
        }
    }
}
