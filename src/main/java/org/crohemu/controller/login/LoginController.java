package org.crohemu.controller.login;

import org.crohemu.ServerConsole;
import org.crohemu.controller.D2Controller;
import org.crohemu.controller.D2MessageReceiver;
import org.crohemu.network.d2protocol.D2Client;
import org.crohemu.network.d2protocol.message.factory.D2MessageFactory;
import org.crohemu.network.d2protocol.message.messages.login.*;
import org.crohemu.network.d2protocol.message.messages.types.IdentificationFailedReason;
import org.crohemu.network.d2protocol.server.D2Server;
import org.crohemu.server.game.GameServer;
import org.crohemu.server.game.config.GameServerConfig;
import org.crohemu.server.login.LoginServer;
import org.crohemu.service.account.AccountService;
import org.crohemu.service.account.AccountService.IdentificationResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.util.Set;

/**
 * Handles the messages related to login and identification.
 */
@D2Controller(serverType = D2Server.Type.LOGIN)
public class LoginController {
    private Logger logger = LoggerFactory.getLogger(LoginController.class);

    @Inject
    ServerConsole serverConsole;

    @Inject
    private AccountService accountService;

    @Inject
    private D2MessageFactory d2MessageFactory;

    @Inject
    private LoginServer loginServer;

    @Inject
    private GameServer gameServer;

    @D2MessageReceiver
    public void identify(IdentificationMessage message, D2Client source) {
        if (source.isIdentified()) {
            logger.error("Client {} tried to connect but is already identified.", source);
            return;
        }

        String[] credentials = new String(message.getCredentials())
                .split(IdentificationMessage.CREDENTIALS_DELIMITER);
        if (credentials.length != 2) {
            logger.error("Client {} tried to identify, but the credentials are in the wrong format", source);
            loginServer.disconnectClient(source);
            return;
        }

        IdentificationResult identificationResult = accountService.identify(credentials[0], credentials[1]);
        switch (identificationResult.result) {
            case FAILURE_INVALID_CREDENTIALS:
                logger.warn("Client {} tried to identify with invalid credentials", source);
                source.sendMessage(new IdentificationFailedMessage(IdentificationFailedReason.INVALID_CREDENTIALS));
                break;
            case FAILURE_BANNED:
                logger.warn("Client {} tried to identify but is banned", source);
                source.sendMessage(new IdentificationFailedBannedMessage(identificationResult.banEndDate.getMillis()));
                break;
            case SUCCESS: // todo make it less ugly
                Set<D2Client> loginServerClientsConnectedOnAccount = loginServer.getClientsConnectedOnAccount(identificationResult.account);
                Set<D2Client> gameServerClientsConnectedOnAccount = gameServer.getClientsConnectedOnAccount(identificationResult.account);
                if (loginServerClientsConnectedOnAccount.size() != 0 || gameServerClientsConnectedOnAccount.size() != 0) {
                    logger.info("Client {} tried to identify but account {} is already connected. Disconnecting the clients.",
                            source, identificationResult.account);
                    loginServerClientsConnectedOnAccount.forEach(d2Client -> loginServer.disconnectClient(d2Client));
                    gameServerClientsConnectedOnAccount.forEach(d2Client -> gameServer.disconnectClient(d2Client));
                    source.sendMessage(new IdentificationFailedMessage(IdentificationFailedReason.KICKED));
                } else {
                    logger.info("Client {} identified successfully", source);
                    source.sendMessage(d2MessageFactory.buildIdentificationSuccessMessage(identificationResult.account));
                    source.sendMessage(d2MessageFactory.buildServersListMessage(identificationResult.account));
                    source.setIdentified(true);
                    source.setAccount(identificationResult.account);
                    serverConsole.info("A player identified as %s", identificationResult.account.getAccountName());
                }
                break;
            default:
                logger.info("Client {} failed to identify for an unknown reason", source);
                break;
        }
    }

    @D2MessageReceiver
    public void selectServer(ServerSelectionMessage message, D2Client source) {
        if (!source.isIdentified()) {
            logger.error("Client {} tried to select a server but is not identified.", source);
            loginServer.disconnectClient(source);
            return;
        }

        if (message.getServerId() != GameServerConfig.getServerId()) {
            logger.error("Client {} tried to select a server but serverId {} is invalid", source, message.getServerId());
            return;
        }

        SelectedServerDataMessage selectedServerDataMessage = d2MessageFactory.buildSelectedServerDataMessage(source.getAccount());
        source.setAuthTicket(selectedServerDataMessage.getTicket());
        gameServer.addFutureClient(source);
        source.sendMessage(selectedServerDataMessage);
        loginServer.disconnectClient(source);
    }
}
