package org.crohemu.controller;

import org.crohemu.network.d2protocol.server.D2Server;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface D2Controller {
    D2Server.Type serverType();
}
