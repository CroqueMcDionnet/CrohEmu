package org.crohemu.controller.game;

import org.crohemu.controller.D2Controller;
import org.crohemu.controller.D2MessageReceiver;
import org.crohemu.network.d2protocol.D2Client;
import org.crohemu.network.d2protocol.message.messages.game.GameContextCreateRequestMessage;
import org.crohemu.network.d2protocol.message.messages.game.context.GameContextCreateMessage;
import org.crohemu.network.d2protocol.message.messages.game.context.GameContextDestroyMessage;
import org.crohemu.network.d2protocol.message.messages.game.map.CurrentMapMessage;
import org.crohemu.network.d2protocol.server.D2Server;
import org.crohemu.server.game.GameServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

@D2Controller(serverType = D2Server.Type.GAME)
public class GameContextController {
    private final Logger logger = LoggerFactory.getLogger(GameContextController.class);

    public static final String MAPS_KEY = "649ae451ca33ec53bbcbcc33becf15f4";

    @Inject
    GameServer gameServer;

    @D2MessageReceiver
    public void requestGameContextCreation(GameContextCreateRequestMessage message, D2Client source) {
        if (!source.isAuthenticated()) {
            logger.error("Client {} request game context creation but is not authenticated!", source);
            gameServer.disconnectClient(source);
            return;
        }

        source.sendMessage(new GameContextDestroyMessage());
        source.sendMessage(new GameContextCreateMessage(GameContextCreateMessage.TYPE_ROLE_PLAY));
        source.sendMessage(new CurrentMapMessage(source.getCurrentCharacter().getCurrentMapId(), MAPS_KEY));
    }
}
