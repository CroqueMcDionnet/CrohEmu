package org.crohemu.controller.game;

import org.crohemu.controller.D2Controller;
import org.crohemu.controller.D2MessageReceiver;
import org.crohemu.model.character.Character;
import org.crohemu.model.world.GameMap;
import org.crohemu.network.d2protocol.D2Client;
import org.crohemu.network.d2protocol.message.messages.game.context.GameContextRemoveElementMessage;
import org.crohemu.network.d2protocol.message.messages.game.map.*;
import org.crohemu.network.d2protocol.message.messages.types.actor.GameRolePlayHumanoidInformation;
import org.crohemu.network.d2protocol.server.D2Server;
import org.crohemu.server.game.GameServer;
import org.crohemu.service.account.CharacterService;
import org.crohemu.service.common.ConversionService;
import org.crohemu.service.world.GameMapService;
import org.crohemu.service.world.path.RolePlayPath;

import javax.inject.Inject;
import java.util.Collections;

@D2Controller(serverType = D2Server.Type.GAME)
public class MapController {
    @Inject
    GameMapService gameMapService;

    @Inject
    CharacterService characterService;

    @Inject
    ConversionService conversionService;

    @Inject
    GameServer gameServer;

    @D2MessageReceiver
    public void requestMapInformation(MapInformationRequestMessage message, D2Client source) {
        MapComplementaryInformationDataMessage dataMessage = new MapComplementaryInformationDataMessage();
        GameMap gameMap = gameMapService.findById(message.getMapId());

        dataMessage.setSubAreaId((short) gameMap.getSubAreaId());
        dataMessage.setMapId(message.getMapId());

        dataMessage.setActors(gameMapService.getActors(gameMap));
        dataMessage.setFightCommonInformationList(Collections.emptyList());
        dataMessage.setHouses(Collections.emptyList());
        dataMessage.setMapObstacles(Collections.emptyList());
        dataMessage.setInteractiveElements(Collections.emptyList());
        dataMessage.setStatedElements(Collections.emptyList());
        dataMessage.setHasAggressiveMonsters(false);

        source.sendMessage(dataMessage);
        GameRolePlayHumanoidInformation information = this.conversionService.characterToGameRolePlayHumanoidInformation(source.getCurrentCharacter());
        source.sendMessage(new GameRolePlayShowActorMessage(information));
    }

    @D2MessageReceiver
    public void requestMapMovement(GameMapMovementRequestMessage message, D2Client source) {
        RolePlayPath path = new RolePlayPath(message.getKeyMovements());
        // todo handle map movement when character is in a fight
        // todo do nothing if character is busy
        GameRolePlayHumanoidInformation characterInformation = this.conversionService.characterToGameRolePlayHumanoidInformation(source.getCurrentCharacter());
        gameServer.sendMap(message.getMapId(), new GameRolePlayShowActorMessage(characterInformation));
        source.getCurrentCharacter().setMovementTargetCellId(path.getEndCellId());
        source.getCurrentCharacter().setCurrentDirection((byte) path.getEndDirection().getValue());
        gameServer.sendMap(message.getMapId(), new GameMapMovementMessage(message.getKeyMovements(), characterInformation.getContextualId()));
    }

    @D2MessageReceiver
    public void confirmMapMovement(GameMapMovementConfirmMessage message, D2Client source) {
        Character currentCharacter = source.getCurrentCharacter();
        currentCharacter.confirmMovement();
        this.characterService.save(currentCharacter);
    }

    @D2MessageReceiver
    public void cancelMapMovement(GameMapMovementCancelMessage message, D2Client source) {
        Character currentCharacter = source.getCurrentCharacter();
        currentCharacter.cancelMovement(message.getCellId());
        this.characterService.save(currentCharacter);
    }

    @D2MessageReceiver
    public void changeMap(ChangeMapMessage message, D2Client source) {
        Character character = source.getCurrentCharacter();
        gameServer.sendMap(character.getCurrentMapId(), new GameContextRemoveElementMessage(character.getCharacterId()));
        characterService.changeMap(character, message.getMapId());
        source.sendMessage(new CurrentMapMessage(character.getCurrentMapId(), GameContextController.MAPS_KEY));
    }
}
