package org.crohemu.controller.game;

import org.crohemu.controller.D2Controller;
import org.crohemu.controller.D2MessageReceiver;
import org.crohemu.network.d2protocol.D2Client;
import org.crohemu.network.d2protocol.message.messages.game.social.*;
import org.crohemu.network.d2protocol.server.D2Server;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

@D2Controller(serverType = D2Server.Type.GAME)
public class SocialController {
    private final Logger logger = LoggerFactory.getLogger(SocialController.class);

    @D2MessageReceiver
    public void getFriendList(FriendsGetListMessage message, D2Client source) {
        // todo
        source.sendMessage(new FriendsListMessage(new ArrayList<>()));
    }

    @D2MessageReceiver
    public void getIgnoredList(IgnoredGetListMessage message, D2Client source) {
        // todo
        source.sendMessage(new IgnoredListMessage(new ArrayList<>()));
    }

    @D2MessageReceiver
    public void getSpouseInformation(SpouseGetInformationMessage message, D2Client source) {
        source.sendMessage(new SpouseStatusMessage(false)); // todo
    }
}
