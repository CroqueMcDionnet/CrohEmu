package org.crohemu.controller;

import net.sf.corn.cps.CPScanner;
import net.sf.corn.cps.ClassFilter;
import net.sf.corn.cps.PackageNameFilter;
import org.crohemu.ApplicationContextProvider;
import org.crohemu.network.d2protocol.message.D2Message;
import org.springframework.stereotype.Component;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Directs message types to controller methods.
 */
@Component
public class MessageControllerRoutes {

    private static final Class<? extends Annotation> CONTROLLER_ANNOTATION = D2Controller.class;

    private boolean initialized = false;

    private Map<Class<? extends D2Message>, MessageController> routes;

    public MessageControllerRoutes() {
        routes = new HashMap<>();
    }

    protected void add(Class<? extends D2Message> type, MessageController controller) {
        routes.put(type, controller);
    }

    public MessageController get(Class<? extends D2Message> type) {
        return routes.get(type);
    }

    public void initialize() {
        if (!initialized) {
            addRoutes();
            initialized = true;
        }
    }

    private void addRoutes() {
        CPScanner.scanClasses(new PackageNameFilter("org.crohemu.controller.*"),
                new ClassFilter().appendAnnotation(CONTROLLER_ANNOTATION)).forEach(this::addRoutes);
    }

    private void addRoutes(Class<?> d2Controller) {
        Object d2ControllerInstance = new ApplicationContextProvider().getApplicationContext().getBean(d2Controller);
        Arrays.asList(d2Controller.getMethods()).forEach(method -> {
            if (method.isAnnotationPresent(D2MessageReceiver.class)) {
                Class<?> messageType = method.getParameterTypes()[0];
                this.add((Class<? extends D2Message>) messageType, (message, source) -> {
                    if (message == null) {
                        return;
                    }
                    try {
                        method.invoke(d2ControllerInstance, messageType.cast(message), source);
                    } catch (IllegalAccessException | InvocationTargetException e) {
                        e.printStackTrace();
                    }
                });
            }
        });
    }
}
