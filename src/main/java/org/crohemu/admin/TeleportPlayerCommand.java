package org.crohemu.admin;

public class TeleportPlayerCommand extends AdminCommand {
    private String characterName;
    private int mapId;
    private int cellId;

    public TeleportPlayerCommand(String characterName, int mapId, int cellId) {
        this.characterName = characterName;
        this.mapId = mapId;
        this.cellId = cellId;
    }

    public String getCharacterName() {
        return characterName;
    }

    public int getMapId() {
        return mapId;
    }

    public int getCellId() {
        return cellId;
    }
}
