package org.crohemu.model.character;

import java.util.List;

public class CharacterBaseLook {
    private List<Integer> bones;
    private List<Integer> skins;
    private List<Integer> scales;
    private List<Integer> colors;

    public List<Integer> getBones() {
        return bones;
    }

    public void setBones(List<Integer> bones) {
        this.bones = bones;
    }

    public List<Integer> getSkins() {
        return skins;
    }

    public void setSkins(List<Integer> skins) {
        this.skins = skins;
    }

    public List<Integer> getScales() {
        return scales;
    }

    public void setScales(List<Integer> scales) {
        this.scales = scales;
    }

    public List<Integer> getColors() {
        return colors;
    }

    public void setColors(List<Integer> colors) {
        this.colors = colors;
    }
}
