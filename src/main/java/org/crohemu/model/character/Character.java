package org.crohemu.model.character;

import org.crohemu.model.character.shortcut.Shortcuts;
import org.crohemu.model.world.path.Direction;
import org.springframework.data.annotation.Transient;

public class Character {

    public final static boolean SEX_MALE = false;
    public final static boolean SEX_FEMALE = true;

    private Long characterId;

    private String name;

    private int level;

    private int breed;

    private boolean sex;

    private int currentMapId;
    private int currentCellId;

    @Transient
    private byte currentDirection = (byte) Direction.EAST.getValue();

    @Transient
    private int movementTargetCellId;

    private CharacterBaseLook look;

    private Inventory inventory;

    private Shortcuts shortcuts;

    public Long getCharacterId() {
        return characterId;
    }

    public void setCharacterId(Long characterId) {
        this.characterId = characterId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevel() {
        return level;
    }


    public void setLevel(int level) {
        this.level = level;
    }

    public int getBreed() {
        return breed;
    }

    public void setBreed(int breed) {
        this.breed = breed;
    }

    public boolean getSex() {
        return sex;
    }

    public void setSex(boolean sex) {
        this.sex = sex;
    }

    public CharacterBaseLook getLook() {
        return look;
    }

    public void setLook(CharacterBaseLook look) {
        this.look = look;
    }

    public Inventory getInventory() {
        return inventory;
    }

    public void setInventory(Inventory inventory) {
        this.inventory = inventory;
    }

    public int getMaxWeight() {
        return 5000; // todo
    }

    public Shortcuts getShortcuts() {
        return shortcuts;
    }

    public void setShortcuts(Shortcuts shortcuts) {
        this.shortcuts = shortcuts;
    }

    public int getCurrentMapId() {
        return currentMapId;
    }

    public void setCurrentMapId(int currentMapId) {
        this.currentMapId = currentMapId;
    }

    public int getCurrentCellId() {
        return currentCellId;
    }

    public void setCurrentCellId(int currentCellId) {
        this.currentCellId = currentCellId;
    }

    public byte getCurrentDirection() {
        return currentDirection;
    }

    public void setCurrentDirection(byte currentDirection) {
        this.currentDirection = currentDirection;
    }

    public void setMovementTargetCellId(int movementTargetCellId) {
        this.movementTargetCellId = movementTargetCellId;
    }

    public int getMovementTargetCellId() {
        return movementTargetCellId;
    }

    public void confirmMovement() {
        currentCellId = movementTargetCellId;
        movementTargetCellId = 0;
    }

    public void cancelMovement(short cellId) {
        currentCellId = cellId;
        movementTargetCellId = 0;
    }
}
