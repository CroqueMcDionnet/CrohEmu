package org.crohemu.model.character.inventory;

import org.crohemu.model.item.effect.InventoryItemEffect;
import org.crohemu.network.d2protocol.D2DataInputStream;
import org.crohemu.network.d2protocol.D2DataOutputStream;
import org.crohemu.network.d2protocol.D2NetworkType;

import java.io.IOException;
import java.util.List;

public class InventoryItem implements D2NetworkType {

    // specific to InventoryItem
    private int position;
    private int inventoryItemGid;
    private List<InventoryItemEffect> effects;
    private int inventoryItemUid;
    private int quantity;

    @Override
    public void serialize(D2DataOutputStream outputStream) {
        try {
            outputStream.writeByte(position);
            outputStream.writeVariableLengthShort(inventoryItemGid);
            outputStream.writeShort(effects.size());
            effects.forEach(effect -> effect.serialize(outputStream));
            outputStream.writeVariableLengthInt(inventoryItemUid);
            outputStream.writeVariableLengthInt(quantity);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deserialize(D2DataInputStream inputStream) {
        // not in use
    }

    @Override
    public boolean isValid() {
        return inventoryItemGid >= 0
                && inventoryItemUid >= 0
                && quantity >= 0;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getInventoryItemGid() {
        return inventoryItemGid;
    }

    public void setInventoryItemGid(int inventoryItemGid) {
        this.inventoryItemGid = inventoryItemGid;
    }

    public List<InventoryItemEffect> getEffects() {
        return effects;
    }

    public void setEffects(List<InventoryItemEffect> effects) {
        this.effects = effects;
    }

    public int getInventoryItemUid() {
        return inventoryItemUid;
    }

    public void setInventoryItemUid(int inventoryItemUid) {
        this.inventoryItemUid = inventoryItemUid;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
