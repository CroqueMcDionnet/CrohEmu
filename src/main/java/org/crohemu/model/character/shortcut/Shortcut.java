package org.crohemu.model.character.shortcut;

import org.crohemu.network.d2protocol.D2DataInputStream;
import org.crohemu.network.d2protocol.D2DataOutputStream;
import org.crohemu.network.d2protocol.D2NetworkType;

import java.io.IOException;

public class Shortcut implements D2NetworkType {

    protected int typeId;

    protected byte slot;

    public Shortcut() {
        this.typeId = 369;
    }

    @Override
    public void serialize(D2DataOutputStream outputStream) {
        try {
            outputStream.writeByte(slot);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deserialize(D2DataInputStream inputStream) {

    }

    @Override
    public boolean isValid() {
        return slot >= 0 && slot <= 99;
    }

    public int getTypeId() {
        return typeId;
    }

    public byte getSlot() {
        return slot;
    }

    public void setSlot(byte slot) {
        this.slot = slot;
    }
}
