package org.crohemu.model.item.effect;

import org.crohemu.network.d2protocol.D2DataInputStream;
import org.crohemu.network.d2protocol.D2DataOutputStream;
import org.crohemu.network.d2protocol.D2NetworkType;

import java.io.IOException;

public class InventoryItemEffect implements D2NetworkType {

    private int typeId;

    private int actionId;

    public InventoryItemEffect(int actionId) {
        this.actionId = actionId;
        this.typeId = 76;
    }

    public int getTypeId() {
        return typeId;
    }

    @Override
    public void serialize(D2DataOutputStream outputStream) {
        try {
            outputStream.writeShort(typeId);
            outputStream.writeVariableLengthShort(actionId);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deserialize(D2DataInputStream inputStream) {
        try {
            actionId = inputStream.readVariableLengthShort();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean isValid() {
        return actionId >= 0;
    }

    public int getActionId() {
        return actionId;
    }

    public void setActionId(int actionId) {
        this.actionId = actionId;
    }
}
