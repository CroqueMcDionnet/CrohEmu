package org.crohemu.model.item;

public class EffectInstance {

    private int effectUid;
    private int effectId;
    private int targetId;
    private String targetMask;
    private int duration;
    private int delay;
    private int random;
    private int group;
    private int modificator;
    private int trigger;
    private int triggers;
    private boolean visibleInTooltip;
    private boolean visibleInBuffUi;
    private boolean visibleInFightLog;
    private String rawZone;
    private int effectElement;

    public int getEffectUid() {
        return effectUid;
    }

    public void setEffectUid(int effectUid) {
        this.effectUid = effectUid;
    }

    public int getEffectId() {
        return effectId;
    }

    public void setEffectId(int effectId) {
        this.effectId = effectId;
    }

    public int getTargetId() {
        return targetId;
    }

    public void setTargetId(int targetId) {
        this.targetId = targetId;
    }

    public String getTargetMask() {
        return targetMask;
    }

    public void setTargetMask(String targetMask) {
        this.targetMask = targetMask;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getDelay() {
        return delay;
    }

    public void setDelay(int delay) {
        this.delay = delay;
    }

    public int getRandom() {
        return random;
    }

    public void setRandom(int random) {
        this.random = random;
    }

    public int getGroup() {
        return group;
    }

    public void setGroup(int group) {
        this.group = group;
    }

    public int getModificator() {
        return modificator;
    }

    public void setModificator(int modificator) {
        this.modificator = modificator;
    }

    public int getTrigger() {
        return trigger;
    }

    public void setTrigger(int trigger) {
        this.trigger = trigger;
    }

    public int getTriggers() {
        return triggers;
    }

    public void setTriggers(int triggers) {
        this.triggers = triggers;
    }

    public boolean isVisibleInTooltip() {
        return visibleInTooltip;
    }

    public void setVisibleInTooltip(boolean visibleInTooltip) {
        this.visibleInTooltip = visibleInTooltip;
    }

    public boolean isVisibleInBuffUi() {
        return visibleInBuffUi;
    }

    public void setVisibleInBuffUi(boolean visibleInBuffUi) {
        this.visibleInBuffUi = visibleInBuffUi;
    }

    public boolean isVisibleInFightLog() {
        return visibleInFightLog;
    }

    public void setVisibleInFightLog(boolean visibleInFightLog) {
        this.visibleInFightLog = visibleInFightLog;
    }

    public String getRawZone() {
        return rawZone;
    }

    public void setRawZone(String rawZone) {
        this.rawZone = rawZone;
    }

    public int getEffectElement() {
        return effectElement;
    }

    public void setEffectElement(int effectElement) {
        this.effectElement = effectElement;
    }
}
