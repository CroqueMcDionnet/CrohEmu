package org.crohemu.model.account;

import org.joda.time.Instant;

/**
 * Subscribing allows the player to have access to all of the game's features.
 * If he does not subscribe, he is limited to a little over the tutorial zone.
 */
public class Subscription {
    /**
     * The subscription's start date. If the player has never subscribed, its value is unix time 0.
     */
    private Instant startDate;

    /**
     * The subscription's end date. If the player has never subscribed, its value is unix time 0.
     */
    private Instant endDate;

    public Instant getStartDate() {
        return startDate;
    }

    public void setStartDate(Instant startDate) {
        this.startDate = startDate;
    }

    public Instant getEndDate() {
        return endDate;
    }

    public void setEndDate(Instant endDate) {
        this.endDate = endDate;
    }
}
