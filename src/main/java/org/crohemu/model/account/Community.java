package org.crohemu.model.account;

/**
 * The community separates the players into countries or regions.
 * It is very likely that there will always be exactly one, as private servers
 * tend to have a small number of players.
 */
public class Community {

    private int communityId;

    public int getCommunityId() {
        return communityId;
    }

    public void setCommunityId(int communityId) {
        this.communityId = communityId;
    }
}
