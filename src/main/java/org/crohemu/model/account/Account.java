package org.crohemu.model.account;

import org.joda.time.Instant;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.crohemu.model.character.Character;
import java.util.List;

@Document(collection = "account")
public class Account {

    @Id
    private String id;

    /**
     * Functional id.
     */
    private int accountId;

    /**
     * The account name that is prompted on login.
     */
    private String accountName;

    /**
     * The account password that is prompted on login.
     */
    private String password;

    /**
     * The nickname is shown to other players. It's like a public accountName.
     */
    private String nickname;

    /**
     * The secret question is asked when performing certain actions such as deleting a character. The action only
     * succeeds if the client answers the corresponding secretAnswer.
     */
    private String secretQuestion;

    /**
     * The answer to the secret question.
     */
    private String secretAnswer;

    /**
     * TODO
     */
    private int havenbagAvailableRoom;

    /**
     * The rights level. Not implemented in the same way as the official server
     */
    private int rights;

    private Instant creationDate;

    private Subscription subscription;
    private Community community;

    /**
     * If banEndDate is after now, the account is banned. If it's before now, it's not.
     */
    private Instant banEndDate;

    private List<Character> characters;

    /**
     * The account's rights level differentiates the admin or moderator from the regular player.
     */
    public enum AccountRights {
        PLAYER(0),
        MODERATOR(1),
        ADMIN(2);

        private int id;

        AccountRights(int id) {
            this.id = id;
        }

        public int getId() {
            return id;
        }
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Instant getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Instant creationDate) {
        this.creationDate = creationDate;
    }

    public Subscription getSubscription() {
        return subscription;
    }

    public void setSubscription(Subscription subscription) {
        this.subscription = subscription;
    }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getSecretQuestion() {
        return secretQuestion;
    }

    public void setSecretQuestion(String secretQuestion) {
        this.secretQuestion = secretQuestion;
    }

    public String getSecretAnswer() {
        return secretAnswer;
    }

    public void setSecretAnswer(String secretAnswer) {
        this.secretAnswer = secretAnswer;
    }

    public int getHavenbagAvailableRoom() {
        return havenbagAvailableRoom;
    }

    public void setHavenbagAvailableRoom(int havenbagAvailableRoom) {
        this.havenbagAvailableRoom = havenbagAvailableRoom;
    }

    public int getRights() {
        return rights;
    }

    public void setRights(int rights) {
        this.rights = rights;
    }

    public Community getCommunity() {
        return community;
    }

    public void setCommunity(Community community) {
        this.community = community;
    }

    public Instant getBanEndDate() {
        return banEndDate;
    }

    public void setBanEndDate(Instant banEndDate) {
        this.banEndDate = banEndDate;
    }

    public List<Character> getCharacters() {
        return characters;
    }

    public void setCharacters(List<Character> characters) {
        this.characters = characters;
    }
}
