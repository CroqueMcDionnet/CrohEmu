package org.crohemu.model.world;

public class Sound {
    private int id;
    private int volume;
    private int criterionId;
    private int silenceMin;
    private int silenceMax;
    private int channel;
    private int type_id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }

    public int getCriterionId() {
        return criterionId;
    }

    public void setCriterionId(int criterionId) {
        this.criterionId = criterionId;
    }

    public int getSilenceMin() {
        return silenceMin;
    }

    public void setSilenceMin(int silenceMin) {
        this.silenceMin = silenceMin;
    }

    public int getSilenceMax() {
        return silenceMax;
    }

    public void setSilenceMax(int silenceMax) {
        this.silenceMax = silenceMax;
    }

    public int getChannel() {
        return channel;
    }

    public void setChannel(int channel) {
        this.channel = channel;
    }

    public int getType_id() {
        return type_id;
    }

    public void setType_id(int type_id) {
        this.type_id = type_id;
    }
}
