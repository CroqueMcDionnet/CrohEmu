package org.crohemu.model.world;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(collection = "Map")
public class GameMap {

    @Id
    private String _id;

    private int id;
    private int posX;
    private int posY;
    private boolean outdoor;
    private int capabilities;
    private int nameId;
    private boolean showNameOnFingerpost;
    private List<Sound> sounds;
    private List<List<Integer>> playlists;
    private int subAreaId;
    private int worldMap;
    private boolean hasPriorityOnWorldMap;
    private boolean isUnderWater;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPosX() {
        return posX;
    }

    public void setPosX(int posX) {
        this.posX = posX;
    }

    public int getPosY() {
        return posY;
    }

    public void setPosY(int posY) {
        this.posY = posY;
    }

    public boolean isOutdoor() {
        return outdoor;
    }

    public void setOutdoor(boolean outdoor) {
        this.outdoor = outdoor;
    }

    public int getCapabilities() {
        return capabilities;
    }

    public void setCapabilities(int capabilities) {
        this.capabilities = capabilities;
    }

    public int getNameId() {
        return nameId;
    }

    public void setNameId(int nameId) {
        this.nameId = nameId;
    }

    public boolean showNameOnFingerpost() {
        return showNameOnFingerpost;
    }

    public void setShowNameOnFingerpost(boolean showNameOnFingerpost) {
        this.showNameOnFingerpost = showNameOnFingerpost;
    }

    public List<Sound> getSounds() {
        return sounds;
    }

    public void setSounds(List<Sound> sounds) {
        this.sounds = sounds;
    }

    public List<List<Integer>> getPlaylists() {
        return playlists;
    }

    public void setPlaylists(List<List<Integer>> playlists) {
        this.playlists = playlists;
    }

    public int getSubAreaId() {
        return subAreaId;
    }

    public void setSubAreaId(int subAreaId) {
        this.subAreaId = subAreaId;
    }

    public int getWorldMap() {
        return worldMap;
    }

    public void setWorldMap(int worldMap) {
        this.worldMap = worldMap;
    }

    public boolean hasPriorityOnWorldMap() {
        return hasPriorityOnWorldMap;
    }

    public void setHasPriorityOnWorldMap(boolean hasPriorityOnWorldMap) {
        this.hasPriorityOnWorldMap = hasPriorityOnWorldMap;
    }

    public boolean isUnderWater() {
        return isUnderWater;
    }

    public void setUnderWater(boolean underWater) {
        isUnderWater = underWater;
    }
}
