package org.crohemu.model.world.path;

public enum Direction {
    EAST(0), SOUTH_EAST(1), SOUTH(2), SOUTH_WEST(3), WEST(4), NORTH_WEST(5), NORTH(6), NORTH_EAST(7);
    public int value;

    Direction(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }
}
