package org.crohemu.model.world;

import java.util.List;

public class Playlist {
    private List<Integer> soundsIds;

    public List<Integer> getSoundsIds() {
        return soundsIds;
    }

    public void setSoundsIds(List<Integer> soundsIds) {
        this.soundsIds = soundsIds;
    }
}
