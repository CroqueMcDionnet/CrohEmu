package org.crohemu.service.account.impl;

import org.crohemu.service.account.CharacterNameGenerator;

import java.util.Random;

public class StaticCharacterNameGenerator implements CharacterNameGenerator {

    private final String[] names = new String[]{
            "Alane", "Conon", "Hidna", "Oenie",
            "Alberin", "Cores", "Hywan", "Orlonall",
            "Alexiley", "Cumma", "Imelisanthann", "Oslastute",
            "Aliana", "Cydrim", "Ithija", "Pethen",
            "Angerna", "Deatris", "Jarrekur", "Quires",
            "Assir", "Drummius", "Jowey", "Remnoco",
            "Audolfy", "Eallan", "Kenwayn", "Rette",
            "Avashleofha", "Edrevin", "Kevouglad", "Roitny",
            "Barri", "Eilen", "Knyger", "Salembas",
            "Bencisp", "Elisold", "Lenharlas", "Sellon",
            "Berond", "Emincal", "Leobersey", "Serbelee",
            "Bertimervin", "Ethet", "Lillas", "Shalloy",
            "Briadeldur", "Fanwy", "Lobarith", "Sheleb",
            "Bruinhil", "Feidding", "Lodac", "Sigbert",
            "Burhisren", "Fiachard", "Madan", "Sirabarchan",
            "Cairyd", "Finleatri", "Marrah", "Stane",
            "Carbough", "Flant", "Medwaynou", "Suadnus",
            "Carella", "Galene", "Melig", "Talabryolfur",
            "Cassetio", "Garampotewena", "Melkola", "Teler",
            "Chebald", "Gerneeten", "Mochsteror", "Thiore",
            "Chennah", "Nichil", "Gildi", "Morit", "Tradalion",
            "Coire", "Gisodach", "Murcherid", "Vagyar",
            "Colberlen", "Glain", "Myndalen", "Valen",
            "Comgalomnan", "Grimtharance", "Nibson", "Viliannar",
            "Comma", "Hersonket", "Nimrothe", "Yorka"
    };

    @Override
    public String generate() {
        return names[new Random().nextInt(names.length)];
    }
}
