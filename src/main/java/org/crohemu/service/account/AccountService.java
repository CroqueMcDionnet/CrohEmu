package org.crohemu.service.account;

import org.crohemu.model.account.Account;
import org.joda.time.Instant;
import org.springframework.stereotype.Service;

@Service
public interface AccountService {

    IdentificationResult identify(String accountName, String password);

    class IdentificationResult {


        public enum Result { SUCCESS, FAILURE_INVALID_CREDENTIALS, FAILURE_BANNED;}
        public Result result;
        public Instant banEndDate;
        public Account account;
        public IdentificationResult(Result result, Instant banEndDate, Account account) {
            this.result = result;
            this.banEndDate = banEndDate;
            this.account = account;
        }
    }

    Account findByCharacterId(long characterId);
}
