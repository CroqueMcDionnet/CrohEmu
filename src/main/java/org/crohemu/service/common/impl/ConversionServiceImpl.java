package org.crohemu.service.common.impl;

import org.crohemu.model.character.Character;
import org.crohemu.model.character.CharacterBaseLook;
import org.crohemu.network.d2protocol.message.messages.types.actor.ActorRestrictionInformation;
import org.crohemu.network.d2protocol.message.messages.types.actor.GameRolePlayHumanoidInformation;
import org.crohemu.network.d2protocol.message.messages.types.entity.EntityDispositionInformation;
import org.crohemu.network.d2protocol.message.messages.types.entity.EntityLook;
import org.crohemu.network.d2protocol.message.messages.types.entity.HumanoidInfo;
import org.crohemu.service.account.AccountService;
import org.crohemu.service.common.ConversionService;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class ConversionServiceImpl implements ConversionService {

    @Inject
    AccountService accountService;

    @Override
    public CharacterBaseLook entityLookToCharacterBaseLook(EntityLook entityLook) {
        CharacterBaseLook characterBaseLook = new CharacterBaseLook();
        characterBaseLook.setBones(Collections.singletonList(entityLook.getBonesId()));
        characterBaseLook.setSkins(entityLook.getSkins());
        characterBaseLook.setScales(entityLook.getScales());
        characterBaseLook.setColors(indexedColorsToColors(entityLook.getIndexedColors()));
        return characterBaseLook;
    }

    @Override
    public List<Integer> indexedColorsToColors(List<Integer> indexedColors) {
        return indexedColors.stream()
                .map(indexedColor -> indexedColor & 0b11111111_11111111_11111111)
                .collect(Collectors.toList());
    }

    @Override
    public List<Integer> colorsToIndexedColors(List<Integer> colors) {
        List<Integer> indexedColors = new ArrayList<>(colors.size());
        for (int index = 0; index < colors.size(); index++) {
            indexedColors.add(colors.get(index) | ((index+1) << 24));
        }
        return indexedColors;
    }

    @Override
    public GameRolePlayHumanoidInformation characterToGameRolePlayHumanoidInformation(Character character) {

        GameRolePlayHumanoidInformation info = new GameRolePlayHumanoidInformation();
        info.setAccountId(this.accountService.findByCharacterId(character.getCharacterId()).getAccountId());
        info.setContextualId(character.getCharacterId()); // todo change this
        info.setName(character.getName());
        info.setEntityDispositionInformation(new EntityDispositionInformation(
                character.getCurrentCellId(), character.getCurrentDirection()));
        info.setEntityLook(characterBaseLookToEntityLook(character.getLook()));
        HumanoidInfo humanoidInfo = new HumanoidInfo();
        humanoidInfo.setSex(character.getSex());
        humanoidInfo.setOptions(Collections.emptyList());
        humanoidInfo.setRestrictions(new ActorRestrictionInformation());
        info.setHumanoidInfo(humanoidInfo); //todo

        return info;
    }

    @Override
    public EntityLook characterBaseLookToEntityLook(CharacterBaseLook characterBaseLook) {

        EntityLook look = new EntityLook();
        look.setBonesId(characterBaseLook.getBones().get(0));
        look.setIndexedColors(colorsToIndexedColors(characterBaseLook.getColors()));
        look.setSubEntities(Collections.emptyList());
        look.setSkins(characterBaseLook.getSkins());
        look.setScales(characterBaseLook.getScales());
        return look;
    }
}
