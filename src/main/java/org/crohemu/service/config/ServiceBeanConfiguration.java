package org.crohemu.service.config;

import org.crohemu.service.account.AccountService;
import org.crohemu.service.account.CharacterNameGenerator;
import org.crohemu.service.account.CharacterService;
import org.crohemu.service.account.impl.AccountServiceImpl;
import org.crohemu.service.account.impl.CharacterServiceImpl;
import org.crohemu.service.account.impl.StaticCharacterNameGenerator;
import org.crohemu.service.common.ConversionService;
import org.crohemu.service.common.impl.ConversionServiceImpl;
import org.crohemu.service.world.GameMapService;
import org.crohemu.service.world.MapPositionService;
import org.crohemu.service.world.MapReferenceService;
import org.crohemu.service.world.impl.GameMapServiceImpl;
import org.crohemu.service.world.impl.MapPositionServiceImpl;
import org.crohemu.service.world.impl.MapReferenceServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ServiceBeanConfiguration {

    @Bean
    AccountService accountService() {
        return new AccountServiceImpl();
    }

    @Bean
    ConversionService conversionService() {
        return new ConversionServiceImpl();
    }

    @Bean
    CharacterService characterService() {
        return new CharacterServiceImpl();
    }

    @Bean
    CharacterNameGenerator characterNameGenerator() {
        return new StaticCharacterNameGenerator();
    }

    @Bean
    MapPositionService mapPositionService() {
        return new MapPositionServiceImpl();
    }

    @Bean
    MapReferenceService mapReferenceService() {
        return new MapReferenceServiceImpl();
    }

    @Bean
    GameMapService gameMapService() {
        return new GameMapServiceImpl();
    }
}
