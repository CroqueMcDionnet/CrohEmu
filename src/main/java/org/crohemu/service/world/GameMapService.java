package org.crohemu.service.world;

import org.crohemu.model.world.GameMap;
import org.crohemu.network.d2protocol.message.messages.types.actor.GameRolePlayActorInformation;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface GameMapService {

    GameMap findById(int mapId);

    boolean mapIdExists(int mapId);

    List<GameRolePlayActorInformation> getActors(GameMap gameMap);

    int getChangeMapCellId(int currentMapId, int targetMapId, int currentCellId);
}
