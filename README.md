# CrohEmu

## Synopsis

CrohEmu is a Dofus 2 server emulator written in Java, using the Spring framework.
It is aimed at being extensible, fast, and well-written.

The project is at a very early stage. As such, it is not possible to install or to run it.
